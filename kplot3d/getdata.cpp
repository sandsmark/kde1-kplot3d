/***************************************************************************
                          FILENAME  -  description                              
                             -------------------                                         
    begin                : DATE                                           
    copyright            : (C) YEAR by AUTHOR                         
    email                : EMAIL                                     
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   * 
 *                                                                         *
 ***************************************************************************/


#include "getdata.h"
#include "qfile.h"
#include "stdio.h"
#include "ling.h"


GetDataFile::GetDataFile( char * fname )
{
	res = *fname;
	tableSize = trSize = 0;
	readFile(fname);
}

GetDataFile::~GetDataFile()
{
}

float GetDataFile::calc( float x, float y )
{
	float dx = x - dtable->x;
	float dy = y - dtable->y;
	float r = dx*dx+dy*dy;
	float nr = r;
	float val = dtable->z;
	for (int i=1; i<tableSize; i++ ) {
		dx = x - (dtable+i)->x;
		dy = y - (dtable+i)->y;
		nr = dx*dx+dy*dy;
		if ( r > nr ) {
			r = nr;
			val = (dtable+i)->z;
		}
	}
	return val;
}

bool GetDataFile::inTriagle ( float x, float y, int tr )
{
}


float GetDataFile::approx ( float x, float y, int tr )
{
}

void GetDataFile::readFile(char * fname)
{
  QFile f(fname);
  if ( !f.open(IO_ReadOnly) ) {
		throw SyntaxException("can't open file");
  };
	dtable = new Point3 [20000];
	//QTextStream t( &f );
	char s[1024];
	int i = 0;
	while ( f.readLine(s,1024) > 0 ) {
		if ( strstr(s,"nan") ) continue;
		sscanf(s,"%g %g %g", &dtable[i].x, &dtable[i].y, &dtable[i].z );
		printf("%g %g %g\n", dtable[i].x, dtable[i].y, dtable[i].z );
		i++;
		tableSize++;
		if (i>20000) break;
	}
	
	
	
}



GetData::GetData(){
	//dict = QDict(5,true,true);
}

GetData::~GetData(){
}

double GetData::calc( char * fname, double x, double y ){
	GetDataFile *df = dict[fname];
	if ( !df ) {
		df = new GetDataFile(fname);
		dict.insert( fname, df );
	}
	return (double)df->calc(x,y);
}
