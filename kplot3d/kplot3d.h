/**********************************************************************
                          kplot3d.h  -  description                              
                             -------------------                                         
    begin                : ��� ��� 11 15:22:15 EEST 1999
                                           
    copyright            : (C) 1999 by Dmitry Poplavsky                         
    email                : dima@linuxfan.com                                     
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   * 
 *                                                                         *
 ***************************************************************************/


#ifndef KPLOT3D_H
#define KPLOT3D_H 
 

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

// include files for QT
#include <qprinter.h>
#include <qpainter.h>

// include files for KDE 
#include <kapp.h> 
#include <ktmainwindow.h> 
#include <kaccel.h>
#include <kiconloader.h>
#include <kmsgbox.h>
#include <kprogress.h>

// application specific includes
#include "kplot3dview.h"
#include "resource.h"

/**
  * This Class is the base class for your application. It sets up the main
  * window and reads the config file as well as providing a menubar, toolbar
  * and statusbar. For the main view, an instance of class Kplot3dView is
  * created which creates your view.
  */
class Kplot3dApp : public KTMainWindow 
{
  Q_OBJECT

public:
  /** construtor */
  Kplot3dApp(); 
  /** destructor */
  ~Kplot3dApp();
  /** initMenuBar creates the menu_bar and inserts the menuitems */
  void initMenuBar();
  /** this creates the toolbars. Change the toobar look and add new toolbars in this
    * function */
  void initToolBar();
  /** setup the statusbar */
  void initStatusBar();
  /** setup the mainview*/
  void initView();
  /** save the app-specific options on slotAppExit or by an Options dialog */
  void saveOptions();
  /** read the app-specific options on init() or by an Options dialog */
  void readOptions();
  /** enable menuentries/toolbar items */
  void enableCommand(int id_);
  /** disable menuentries/toolbar items */
  void disableCommand(int id_);
  /** overloaded for Message box on last window exit */
  bool queryExit();

 public slots:
  /** switch argument for slot selection by menu or toolbar ID */
  void commandCallback(int id_);
  /** switch argument for Statusbar help entries on slot selection */
  void statusCallback(int id_);
  /** open a new application window */
  void slotFileNewWindow();
  /** open a document */
  void slotFileOpen();
  /** save a document */
  void slotFileSaveImage();
  void slotFileSaveData();
  /** save a document under a different filename*/
  void slotFileSaveAs();
  /** close the actual file */
  void slotFileClose();
  /** print the actual file */
  void slotFilePrint();
  /** close the current window */
  void slotFileCloseWindow();
  /** exits the application */
  void slotFileQuit();
  /** change the status message to text */
  void slotStatusMsg(const char *text);
  /** change the status message of the whole statusbar temporary */
  void slotStatusHelpMsg(const char *text);
  /**  set ranges for  x and y*/
  void slotRanges();
  /** set colors */
  void slotColors();
  /** Toggle surface type */
  void slotToggle();
  /** Set grid mode */
  void slotOptionsGeneral();

private:
  /** file_menu contains all items of the menubar entry "File" */
  QPopupMenu *file_menu;
  /** options_menu contains all items of the menubar entry "View" */
  QPopupMenu *options_menu;
  /** help_menu contains all items of the menubar entry "Help" */
  QPopupMenu *help_menu;
  /** view is the main widget which represents your working area. The View
    * class should handle all events of the view widget.  It is kept empty so
    * you can create your view according to your application's needs by
    * changing the view class. */
  Kplot3dView *view;
  /** doc represents your actual document and is created only once. It keeps
    * information such as filename and does the serialization of your files.
    */
//  Kplot3dDoc *doc;
  /** flag if toolbar is visible or not. Used for kconfig and checking the
    * view-menu entry view toolbar. bViewStatusbar does the same for the
    * statusbar.
    */
  KProgress *progress;
  bool bViewToolbar;
  bool bViewStatusbar;
  /** used for KConfig to store and restore menubar position. Same with
    * tool_bar_pos. If more menubars or toolbars are created, you should add
    * positions as well and implement them in saveOptions() and readOptions().
    */
  KMenuBar::menuPosition menu_bar_pos;
  KToolBar::BarPosition tool_bar_pos;
};   
 
#endif // KPLOT3D_H
 
 
 
 
 
 
 
 




























