#ifndef DataRanges_H
#define DataRanges_H


class DataRanges
{
  public:
  DataRanges() {};
  ~DataRanges() {};
  double x1,x2,y1,y2;
  double phi,psi;
  int surf_nx, surf_ny, rot_nx, rot_ny, line_nx, line_ny;
};

#endif
