#include <qwidget.h>
#include <qpainter.h>
#include "ShowColor.h"

ShowColor::ShowColor( QWidget* parent, QColor&  cUp , QColor&  cDown,QColor&  cLinesUp , QColor&  cLinesDown )
	: QWidget( parent )
{
	colors[0] = cUp;
	colors[1] = cDown;
	colors[2] = cLinesUp;
	colors[3] = cLinesDown;
	cflag = tflag = 0;
	
	r = colors[0].red();
	g = colors[0].green();
	b = colors[0].blue();
	
	repaint(false);
}	

void ShowColor::setDefault()
{
	colors[0] = QColor(0,0,200);
	colors[1] = QColor(128,128,128);
	colors[2] = QColor(80,80,80);
	colors[3] = QColor(0,160,0);
	
	r = colors[cflag+tflag].red();
	g = colors[cflag+tflag].green();
	b = colors[cflag+tflag].blue();
	
	emit changedR(r);
	emit changedG(g);
	emit changedB(b);
	
	repaint(false);
	
}

void ShowColor::paintEvent( QPaintEvent * )
{
	QPainter p;
	p.begin( this );
	p.fillRect(0, 0, width(), height()/2, QBrush(colors[tflag]));	
	p.fillRect(0, height()/2, width(), height(), QBrush(colors[tflag+1]));	
	p.setPen( QColor(250,250,150) );
	if ( cflag == 0 ) p.drawRect(0, 0, width(), height()/2);
	else p.drawRect(0, height()/2, width(), height());
	p.end();
};


void ShowColor::setRgb( int R, int G, int B)
{
	r = R; g = G; b = B;
	colors[ cflag+tflag ] .setRgb(r,g,b);
	repaint(false);
	emit changedR(r);
	emit changedG(g);
	emit changedB(b);
};

void ShowColor::setR(int R) {
           r = R;
           colors[ cflag+tflag ] .setRgb(r,g,b);
           repaint(false);
           emit changedR(r);
};

void ShowColor::setG(int G) {
           g = G;
           colors[ cflag+tflag ] .setRgb(r,g,b);
           repaint(false);
           emit changedR(r);
};

void ShowColor::setB(int B) {
           b = B;
           colors[ cflag+tflag ] .setRgb(r,g,b);
           repaint(false);
           emit changedR(r);
};








