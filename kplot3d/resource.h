/***************************************************************************
                          resource.h  -  description                              
                             -------------------                                         
    begin                : ��� ��� 11 15:22:15 EEST 1999
                                           
    copyright            : (C) 1999 by Dmitry Poplavsky                         
    email                : dima@linuxfan.com                                     
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   * 
 *                                                                         *
 ***************************************************************************/


#ifndef RESSOURCE_H
#define RESSOURCE_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

///////////////////////////////////////////////////////////////////
// resource.h  -- contains macros used for commands


///////////////////////////////////////////////////////////////////
// COMMAND VALUES FOR MENUBAR AND TOOLBAR ENTRIES


///////////////////////////////////////////////////////////////////
// File-menu entries
#define ID_FILE_NEW_WINDOW          10010
#define ID_FILE_NEW                 10020
#define ID_FILE_OPEN                10030

#define ID_FILE_SAVE_IMAGE                10050
#define ID_FILE_SAVE_DATA                10055
#define ID_FILE_SAVE_AS             10060
#define ID_FILE_CLOSE               10070

#define ID_FILE_PRINT               10080

#define ID_FILE_CLOSE_WINDOW        10090
#define ID_FILE_QUIT                10100


//////////////////////////////////////////////////////////////////////
// View-menu entries
#define ID_RANGES             12010
#define ID_COLORS           12020
#define ID_OPTIONS             12030
#define ID_GENERAL	             12040
#define ID_TOGGLE						 12070

///////////////////////////////////////////////////////////////////
// Help-menu entries
#define ID_HELP                     1002

///////////////////////////////////////////////////////////////////
// General application values
#define ID_STATUS_MSG               1001
#define ID_STATUS_PROGRESS  1002
#define ID_STATUS_SPACE			     1003

#define IDS_DEFAULT                 i18n("Ready.")

#define IDS_APP_ABOUT               "Kplot3d\nVersion " VERSION"\n\nby Dmitry Poplavsky        \ndima@linuxfan.com"






///////////////////////////////////////////////////////////////////
// MACROS FOR THE CONNECT OF YOUR SIGNALS TO CORRESPONDENT SLOTS 
// IN YOUR MAIN IMPLEMENTATION OF MENUBAR AND TOOLBAR


///////////////////////////////////////////////////////////////////
// MENU CONNECTS
#define CONNECT_CMD(submenu)           connect(submenu, SIGNAL(activated(int)), SLOT(commandCallback(int)));connect(submenu,SIGNAL(highlighted(int)), SLOT(statusCallback(int)))


///////////////////////////////////////////////////////////////////
// TOOLBAR CONNECT  
#define CONNECT_TOOLBAR(ToolBar)        connect(ToolBar, SIGNAL(clicked(int)), SLOT(commandCallback(int)));connect(ToolBar, SIGNAL(highlighted(int,bool)), SLOT(statusCallback(int)))

///////////////////////////////////////////////////////////////////
// Create cases for entries and connect them with their functions
#define ON_CMD(id, cmd_class_function)   case id:cmd_class_function ;break;

///////////////////////////////////////////////////////////////////
// Create cases for entries and connect them to change statusBar entry
#define ON_STATUS_MSG(id, message)     case id:  slotStatusHelpMsg(message);break;

#endif // RESOURCE_H
















