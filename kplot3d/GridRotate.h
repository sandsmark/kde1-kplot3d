#ifndef GRIDROTATE_H
#define GRIDROTATE_H
#include <qwidget.h>

class GridRotate : public QWidget
{
    Q_OBJECT
public:
       GridRotate( QWidget *parent=0, const char *name=0 );
       ~GridRotate() {};
public slots:
       void changePhi( int newPhi );
       void changePsi( int newPsi );
protected:
       void paintEvent( QPaintEvent * );
private:
	double phi,psi;
};

#endif