/***************************************************************************
                          FILENAME  -  description                              
                             -------------------                                         
    begin                : DATE                                           
    copyright            : (C) YEAR by AUTHOR                         
    email                : EMAIL                                     
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   * 
 *                                                                         *
 ***************************************************************************/


#ifndef AXESOPTIONS_H
#define AXESOPTIONS_H

//Generated area. DO NOT EDIT!!!(begin)
#include <qwidget.h>
#include <qpushbutton.h>
#include <qgroupbox.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qcheckbox.h>
#include <kseparator.h>
#include <kcolorbtn.h>
//Generated area. DO NOT EDIT!!!(end)

#include <qdialog.h>

/**
  *@author Dmitry Poplavsky
  */

struct AxesData {
	bool useGrid;
	double  gridStep;
	bool addAxes,moveCenter,showPoints,showNumbers;
	double pointsStep;
	bool showBox;
	QColor boxColor;	
};



class AxesOptions : public QDialog  {
   Q_OBJECT
public: 
	AxesOptions(QWidget *parent=0, const char *name=0);
	void setData( AxesData& data );
	void getData( AxesData& data );
	~AxesOptions();
	
protected: 
	void initDialog();
	//Generated area. DO NOT EDIT!!!(begin)
	QPushButton *ok_button;
	QPushButton *cancel_button;
	QGroupBox *QGroupBox_1;
	QGroupBox *QGroupBox_2;
	QLabel *QLabel_1;
	QLineEdit *line_gridStep;
	QCheckBox *check_useGrid;
	QCheckBox *check_addAxes;
	QLabel *QLabel_2;
	QLineEdit *line_pointsStep;
	QCheckBox *check_showPoints;
	QCheckBox *check_showNumbers;
	KSeparator *KSeparator_1;
	KSeparator *KSeparator_2;
	QGroupBox *QGroupBox_3;
	QCheckBox *check_showBox;
	KColorButton *boxColor;
	QCheckBox *check_center;
	//Generated area. DO NOT EDIT!!!(end)
protected slots:
	void slotShowPoints();
	void slotUseGrid();
	void slotAddAxes();
	
private: 
};

#endif
