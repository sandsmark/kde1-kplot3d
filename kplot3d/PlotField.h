#include <qwidget.h>
#include "ling.h"
#include "DataRanges.h"
#include "axesoptions.h"

struct Point2
{
   int x, y;
};

class QImage;
class QFrame;
class QColor;
class QPixmap;
class QString;


class PlotField : public QFrame
{
    Q_OBJECT
public:
  PlotField( QWidget *parent=0, const char *name=0 );
	double f(double x, double y);
	void saveImg(char * fname);
	void saveData(char * fname);
	void setRanges( DataRanges Data);
	void getRanges( DataRanges& Data );
	QPixmap * getPix() { return pix; }
	
	QColor cUp, cDown;
	QColor cLineUp,cLineDown;
	int contrast;
	AxesData axesData;
		
	enum SurfaceType { lined, shaded };
	SurfaceType surfaceType; // lines of shades	
	
public slots:
  void setFunc( const char *func );
	void Paint() { PrepareImage(); repaint(false); };
	void newPaint ( const char * func ) { setFunc(func); Paint();};
	void stopPaint();
	void PrepareImage(double* table = NULL);
	void setSurfaceType( SurfaceType t );	
	void toggleSurfaceType();			
	
signals:
	void done_perc(int);
	void startPaint();	
	void showStatus(const char* message);
	void setLine(int);

protected:
        virtual void paintEvent( QPaintEvent * );
private:

	virtual void mouseMoveEvent(QMouseEvent *ev);
	virtual void mousePressEvent(QMouseEvent *ev);
	virtual void mouseReleaseEvent(QMouseEvent *ev);
	bool mousePressed;
	QPoint currentPos;
        QString funcStr;
	bool painting;
	double x1,y1,x2,y2,fmin,fmax;
	int sizex , sizey;
	int n1,n2, rot_n1, rot_n2, surf_n1, surf_n2, line_n1, line_n2;
	double phi,psi;
	
	int *YMax, *YMin; // need for DrawLine() and plot3dLines()
	
	void plot3dShaded(QPainter *, double* table = NULL);
	void plot3dLines(QPainter *);
	void plot3d(QPainter *, double* table = NULL);
	void DrawLine (QPainter *, Point2, Point2);
	Ling ling;
	QPixmap * pix;
	double *rot_tab;
	QImage *intro;
};

