/***************************************************************************
                          kplot3dview.cpp  -  description                              
                             -------------------                                         
    begin                : ��� ��� 11 15:22:15 EEST 1999
                                           
    copyright            : (C) 1999 by Dmitry Poplavsky                         
    email                : dima@linuxfan.com                                     
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   * 
 *                                                                         *
 ***************************************************************************/


#include <kplot3dview.h>


#include <qmenubar.h>
#include <qpopupmenu.h>
#include <qlayout.h>
#include <qlabel.h>
#include <qmessagebox.h>
#include <qstring.h>
#include <kfiledialog.h>
#include "DataRanges.h"
#include <qaccel.h>
#include "SetColor.h"
#include "axesoptions.h"




Kplot3dView::Kplot3dView(KApplication* a,QWidget *parent)
	: QWidget(parent){
	  setMinimumSize( 400, 300 );
      QBoxLayout *topLayout = new QVBoxLayout( this, 5 );

      QBoxLayout * workLayout = new QVBoxLayout();
      topLayout->addLayout( workLayout );

      funcStr = new QComboBox(true, this, "funStr");

      QHBoxLayout * upLayout = new QHBoxLayout();
      workLayout -> addLayout(upLayout);
      //funcStr->setText("");
      upLayout->addWidget(funcStr,10);
      funcStr->setMinimumHeight( 25 );
      funcStr->setMaximumHeight( 25 );
      funcStr->setAutoCompletion(true);

      repaintButton = new QPushButton(i18n("repaint"),this);
      repaintButton->setMinimumHeight( 25 );
      repaintButton->setMaximumHeight( 25 );
      repaintButton->setMinimumWidth( QLabel(i18n("repaint")).sizeHint().width() );
      repaintButton->setMaximumWidth( QLabel(i18n("repaint")).sizeHint().width()+10 );
      upLayout->addWidget(repaintButton,1);

      plotField = new PlotField( this );
      plotField->setFunc("");
      workLayout->addWidget(plotField,10);


      funcStr->setFocus();

//      connect ( funcStr, SIGNAL(textChanged(const char *)), plotField, SLOT(setFunc(const char *)) );
//      connect ( funcStr, SIGNAL(returnPressed()), plotField, SLOT(Paint()));
      connect ( funcStr, SIGNAL( activated(const char *)), plotField, SLOT(newPaint(const char *)) );
      connect ( repaintButton, SIGNAL( pressed() ), plotField, SLOT(Paint()));
      connect ( plotField, SIGNAL(done_perc(int)), this, SLOT(slotDone_perc(int)) );
      connect ( plotField, SIGNAL( setLine(int) ), this, SLOT(setLine(int)));

      rdata.y1 = rdata.x1 = -3.0;
      rdata.x2 = rdata.y2 = 3.0;
      rdata.phi = 30.0;
      rdata.psi = 20.0;
      rdata.surf_nx = rdata.surf_ny = 100;
      rdata.line_nx = rdata.line_ny = 40;
      rdata.rot_nx = rdata.rot_ny = 30;
}

  void Kplot3dView::saveImg()
   {
      QString f = KFileDialog::getSaveFileName( 0, "*.bmp", this );
      if ( !f.isEmpty() ) {
         plotField->saveImg( f.data() );
      }
   }

   void Kplot3dView::saveData()
   {
      QString f = KFileDialog::getSaveFileName( 0, "*.xyz", this );
      if ( !f.isEmpty() ) {
         plotField->saveData( f.data() );
      }
   }


   void Kplot3dView::slotOptions()
   {
      options = new OptionsDialog(this);
      plotField->getRanges( rdata );
      options->setData( rdata );
   //   options->exec();
      if ( options->exec() )
      {
         options->getData(rdata);
         plotField->setRanges( rdata );
         plotField->PrepareImage();
         plotField->repaint();
      }
      delete options;
   }


   void Kplot3dView::slotOptionsAxes()
   {
      AxesOptions  options(this,"Options");
      options.setData( plotField->axesData );
      if ( options.exec() ) {
      	options.getData( plotField->axesData );	
      	plotField->PrepareImage();
        plotField->repaint();
      }
   }




   void Kplot3dView::slotSetColor()
   {
        SetColor *dcolor = new SetColor(this, plotField->cUp, plotField->cDown,
        		plotField->cLineUp, plotField->cLineDown, plotField->contrast);
        if  ( dcolor->exec() )
            {
                plotField->cUp = dcolor->cUp();
                plotField->cDown = dcolor->cDown();
                plotField->cLineUp = dcolor->cLinesUp();
                plotField->cLineDown = dcolor->cLinesDown();
                plotField->contrast = dcolor->contrast();

                plotField->PrepareImage();
                plotField->repaint();
            }
        delete dcolor;
   }
void Kplot3dView::slotDone_perc(int perc)
{
	if ( perc ) {
	QString s = "";
	s.sprintf("%s %i%c", i18n("done"), perc,'%');
	emit showStatus(s.data());
	}
}


Kplot3dView::~Kplot3dView(){
}


































