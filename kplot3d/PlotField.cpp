#include "PlotField.h"
#include "Vector.h"
#include <qpainter.h>
#include <qcolor.h>
#include <qpointarray.h>
#include <qprogressbar.h>
#include <qfont.h>
#include <qfile.h>
#include <qtextstream.h>
#include <qimage.h>

#include <kapp.h>
#include <kimgio.h>

#include "DataRanges.h"
#include <math.h>
#include <stdio.h>


#define NAN -9.790384327e12
#define L_NAN 9000
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))


PlotField::PlotField( QWidget *parent, const char *name )
        : QFrame( parent, name )
{
		setFrameStyle ( Box|Sunken );
		setMidLineWidth ( 2 );
    painting = false;
    mousePressed = false;
    axesData.addAxes = true;
    axesData.showPoints = true;
    axesData.showBox = false;
    axesData.showNumbers = true;
    axesData.moveCenter = true;
    axesData.useGrid = true;

    axesData.gridStep = 1.0;
    axesData.pointsStep = 1.0;

    surfaceType = lined;
    x1 = y1 = -3;
    x2 = y2 = 3;
    n1 = n2 = 50;
    rot_n1 = rot_n2 = 30;
    line_n1 = line_n2 = 50;
    surf_n1 = surf_n2 = 100;
    setFunc("");
    //setMinimumSize(200,200);
    phi = 30 * PI/180;
    psi = 20 * PI/180;
//    sizex = sizey = 400;
    sizex = 450;
    sizey = 251;
    cUp = QColor( 0 , 0 , 200);
    cDown = QColor(128, 128, 128);
    cLineUp = QColor(128, 128, 128);
    cLineDown = QColor(0, 128, 0);

    //qInitJpegIO();
    kimgioRegister();

    QString introName = kapp->kde_datadir()+"/kplot3d/pics/intro.jpg";
    intro = new QImage ( introName.data() );

    pix = new QPixmap( sizex , sizey );
    PrepareImage();
}

void PlotField::setRanges( DataRanges data )
{
    x1 = data.x1;
    y1 = data.y1;
    x2 = data.x2;
    y2 = data.y2;
    surf_n1 = data.surf_nx;
    surf_n2 = data.surf_ny;
    rot_n1 = data.rot_nx;
    rot_n2 = data.rot_ny;
    line_n1 = data.line_nx;
    line_n2 = data.line_ny;
    n1 = surf_n1;
    n2 = surf_n2;

    phi = data.phi * PI / 180;
    psi = data.psi * PI / 180;
}

void PlotField::setSurfaceType( SurfaceType t )
{
		surfaceType = t;
		if ( t == lined ) {
			n1 = line_n1;
			n2 = line_n2;
		} else {
			n1 = surf_n1;
			n2 = surf_n2;
		}
}

void PlotField::toggleSurfaceType()
{
		if ( surfaceType == lined ) {
			surfaceType = shaded;
			n1 = surf_n1;
			n2 = surf_n2;
		} else {
			surfaceType = lined;
			n1 = line_n1;
			n2 = line_n2;
		}
		Paint();
}


void PlotField::getRanges( DataRanges& data )
{
    data.x1 = x1;
    data.y1 = y1;
    data.x2 = x2;
    data.y2 = y2;
    data.surf_nx = surf_n1;
    data.surf_ny = surf_n2;
    data.rot_nx = rot_n1;
    data.rot_ny = rot_n2;
    data.phi  = int(phi/ PI * 180);
    data.psi  = int(psi / PI * 180);
}


void PlotField::setFunc(const char *func)
{
    funcStr = func;
    ling.setFunc(func);
}


void PlotField::saveImg(char * fname)
{
    pix->save( fname, "BMP" );
}

void PlotField::saveData(char * fname)
{
	QFile f(fname);
	f.open(IO_ReadWrite);
	QTextStream t( &f );
	
	double hx = (x2-x1)/n1;
  double hy = (y2-y1)/n2;
  double z;
	
	for ( double x = x1; x<x2; x+=hx )
		for ( double y = y1; y<y2; y+=hy ) {
				t << x << " " << y << " ";
				try {
					z = ling.calc(x,y);
					t << z << "\n";
				} catch (...) {
					t << "nan\n";
				}
		}
	
	f.close();
		
}


void PlotField::stopPaint()
{
    painting = false;
}


void PlotField::PrepareImage(double * table = NULL)
{
     
    QPainter tmpPainter;
     
    // Found fmin and fmax
    double tt;
    if ( x2<x1 ) {  // swap x1 and x2
        tt = x2;
        x2 = x1;
        x1 = tt;
    }
    if ( y2<y1 ) {  // swap y1 and y2
        tt = y2;
        y2 = y1;
        y1 = tt;
    }
    if ( !funcStr.isEmpty() ) {
        try  // check for syntax errors
        { 
            fmax = fmin = ling.calc( x1, y1 );
        } 
        catch (SyntaxException e) {
            emit showStatus( e.s );
            emit setLine( e.pos );
            return;
        } 
				catch ( RangeException e ) { fmin = fmax = 0; }
        float dx = (x2-x1) / 26.0;
        float dy = (y2-y1) / 26.0;
        for ( double x = x1; x < x2; x+= dx )
            for ( double y = y1; y < y2; y+=dy )
            {
				try {
                    tt = ling.calc(x,y);
            	    if (tt > fmax)  fmax = tt;
            	    if (tt < fmin)  fmin = tt;
				} catch ( RangeException e ) { }
    }
    if (( sizex != width()-10 ) || ( sizey != height()-10 ))
       {
           sizex = width()-10;
           sizey = height()-10;
           delete pix;
           pix = new QPixmap(sizex, sizey);
       }
    }
    if ( fmin == fmax ) fmax+=0.1;

    pix->fill(this,1,1);
//    pix->fill(QColor(170,170,170));
    tmpPainter.begin(pix);
    if ( !funcStr.isEmpty() ) plot3d(&tmpPainter, table);
    else { // show intro
    	
    	// debug( introName.data() );
    	tmpPainter.drawImage(0,0,*intro);
    }
    tmpPainter.end();
}


double PlotField::f(double x, double y)
{
    return min(max((ling.calc(x,y) - fmin) / (fmax - fmin)*0.8 - 0.4,-0.5),0.5) ;
}


void PlotField::paintEvent( QPaintEvent * e )
{
    QPainter p;
    p.begin( this );
     
    //p.drawRect(  this->rect() );
    if (( sizex != width()-10 ) || ( sizey != height()-10 ))
        PrepareImage();
    p.drawPixmap( 5, 5, *pix );
    p.end();
		QFrame::paintEvent(e);

}


void PlotField::plot3d(QPainter *p, double* table = NULL )
{
 if  ( surfaceType == shaded )
 		 plot3dShaded(p,table);
 else
 		 plot3dLines(p);	
		
}


void PlotField::DrawLine(QPainter *p, Point2 p1, Point2 p2)
{
    int dx = abs( p2.x - p1.x );
    int dy = abs( p2.y - p1.y );
    int sx = p2.x >= p1.x ? 1 : -1;
    int sy = p2.y >= p1.y ? 1 : -1;

    if ( dy <= dx ) {
        int d = -dx;
        int d1 = dy << 1;
        int d2 = ( dy - dx ) << 1;
        for ( int x = p1.x, y = p1.y, i = 0; i<=dx; i++,x+=sx ) {
            if ( YMin[x] == L_NAN ) {
                p->setPen(cLineUp);
                p->drawPoint(x,y);
                YMin[x] = YMax[x] = y;
            }
            else
            if ( y < YMin[x] ) {
                p->setPen(cLineUp);
                p->drawPoint(x,y);
                YMin[x] = y;
            }
            if ( y > YMax[x] ) {
                p->setPen(cLineDown);
                p->drawPoint(x,y);
                YMax[x] = y;
            }
            if ( d > 0 ) {
                d+=d2; y+=sy;
            } else d+=d1;

        }
    }
    else
    {
        int d = -dy;
        int d1 = dx << 1;
        int d2 = ( dx - dy ) << 1;
        int m1 = YMin [p1.x];
        int m2 = YMax [p1.x];

        for ( int x = p1.x, y = p1.y, i = 0; i<=dy; i++,y+=sy ) {
            if ( YMin[x] == L_NAN ) {
                p->setPen(cLineUp);
                p->drawPoint(x,y);
                YMin[x] = YMax[x] = y;
            }
            else
            if ( y < m1 ) {
                p->setPen(cLineUp);
                p->drawPoint(x,y);
                if ( y < YMin[x] ) YMin[x] = y;
            }
            if ( y > m2 ) {
                p->setPen(cLineDown);
                p->drawPoint(x,y);
                if ( y > YMax[x] ) YMax[x] = y;
            }
            if ( d > 0 ) {
                d+=d2; x+=sx;
            } else d+=d1;

        }
    }


}


void swap ( double & x, double& y )
{
		double t = x;
		x = y;
		y = t;
}



void PlotField::plot3dLines( QPainter *p )
{

    YMax = new int [sizex];
    YMin = new int [sizex];
    Point2 * CurLine = new Point2 [line_n1];
    Point2 * NextLine = new Point2 [line_n1];

    double sphi = sin(phi);
    double cphi = cos(phi);
    double spsi = sin(psi);
    double cpsi = cos(psi);

    Vector e1 (cphi, sphi, 0);
    Vector e2 (spsi*sphi, -spsi*cphi, cpsi);
//    Vector e3 (sphi*cpsi, -cphi*cpsi, -spsi);

    double old_x1 = x1, old_x2 = x2, old_y1 = y1, old_y2 = y2;
    x1 = min( old_x1, old_x2 );
    x2 = max( old_x1, old_x2 );
    y1 = min( old_y1, old_y2 );
    y2 = max( old_y1, old_y2 );
//    if ( e3.x < 0 ) swap(x1,x2);


    double xmin=(e1.x>=0?x1:x2)*e1.x+(e1.y>=0?y1:y2)*e1.y;
    double xmax=(e1.x>=0?x2:x1)*e1.x+(e1.y>=0?y2:y1)*e1.y;
    double ymin=(e2.x>=0?x1:x2)*e2.x+(e2.y>=0?y1:y2)*e2.y;
    double ymax=(e2.x>=0?x2:x1)*e2.x+(e2.y>=0?y2:y1)*e2.y;

    if ( cphi < 0 )	swap( y1, y2 );
    if ( sphi < 0 ) swap( x1, x2 );

    int i,j;
    double x,y;

    double hx = (x2-x1)/line_n1;
    double hy = (y2-y1)/line_n2;

    if ( e2.z>=0 ) {
       ymax += e2.z/2;
       ymin -= e2.z/2;
    }
    else {
       ymin += e2.z/2;
       ymax -= e2.z/2;
    }

    double ax = -sizex*xmin/(xmax-xmin);
    double bx = sizex/(xmax-xmin);
    double ay = -sizey*ymin/(ymax-ymin);
    double by = -sizey/(ymax-ymin);

    for (i = 0; i< sizex; i++)
        YMax[i] = YMin[i] = L_NAN;
    for ( i=0; i<line_n1; i++ ) {
        x = x1 + i*hx;
        y = y1 + (line_n2-1)*hy;
        CurLine[i].x = (int)(ax+bx*(x*e1.x+y*e1.y));
        try { CurLine[i].y = (int)(ay+by*(x*e2.x+y*e2.y+f(x,y)*e2.z)); }
        catch ( RangeException e ) { CurLine[i].y = L_NAN; }
    }
    for ( i=line_n2-1; i>-1; i-- ) {
        for ( j = 0; j<line_n1-1; j++)
        	if ( CurLine[j].y!=L_NAN && CurLine[j+1].y!=L_NAN )
            DrawLine(p, CurLine[j], CurLine[j+1] );
        if (i>0) {
            for (j=0; j<line_n1; j++) {
                x = x1 + j*hx;
                y = y1 + (i-1)*hy;
                NextLine[j].x = (int)(ax+bx*(x*e1.x+y*e1.y));
                try { NextLine[j].y = (int)(ay+by*(x*e2.x+y*e2.y+f(x,y)*e2.z)); }
                catch ( RangeException e ) { NextLine[j].y = L_NAN; }
                if ( CurLine[j].y!=L_NAN && NextLine[j].y!=L_NAN )
		                DrawLine(p,CurLine[j],NextLine[j]);
                CurLine[j] = NextLine[j];
            }
        }
    }
    delete CurLine;
    delete NextLine;
    delete YMin;
    delete YMax;

    x1 = old_x1;
    x2 = old_x2;
    y1 = old_y1;
    y2 = old_y2;
}

// need if use before builded table and look to surface when sphi<0 or spsi<0
#define translInd(x,y) ( ( xOrder>0 ? (x):(n1-x-1) )+( yOrder>0?(y)*n1:(n2-y-1)*n1 ))

#define inside(x,a1,a2) ( ( x<=max(a1,a2) && x>=min(a1,a2))? true : false )

void PlotField::plot3dShaded(QPainter *p, double* table = NULL )
{
    painting = true;

    QColor* pcolor= new QColor(100,100,100);
    QPointArray* parr = new QPointArray ( 4 );
     
    Point2 * CurLine = new Point2 [n1];
    Point2 * NextLine = new Point2 [n1];  
    Vector * CurPoint = new Vector [n1];  
    Vector * NextPoint = new Vector [n1];  
     
    double sphi = sin(phi);
    double cphi = cos(phi);
    double spsi = sin(psi);
    double cpsi = cos(psi);
     
    Vector e1 (cphi, sphi, 0);
    Vector e2 (spsi*sphi, -spsi*cphi, cpsi);
    Vector e3 (sphi*cpsi, -cphi*cpsi, -spsi);

    double old_x1 = x1, old_x2 = x2, old_y1 = y1, old_y2 = y2;
    x1 = min( old_x1, old_x2 );
    x2 = max( old_x1, old_x2 );
    y1 = min( old_y1, old_y2 );
    y2 = max( old_y1, old_y2 );
     
    double xmin=(e1.x>=0?x1:x2)*e1.x+(e1.y>=0?y1:y2)*e1.y;
    double xmax=(e1.x>=0?x2:x1)*e1.x+(e1.y>=0?y2:y1)*e1.y;
    double ymin=(e2.x>=0?x1:x2)*e2.x+(e2.y>=0?y1:y2)*e2.y;
    double ymax=(e2.x>=0?x2:x1)*e2.x+(e2.y>=0?y2:y1)*e2.y;

    int x0 = int( double(n1)*(-xmin)/(xmax-xmin)); // ned for plotting axes
    int y0 = int( double(n2)*(-ymin)/(ymax-ymin));

    if ( axesData.moveCenter ) {
    	x0 = n1/2;
    	y0 = n2/2;
    }


    QPen xAxe(QColor("yellow"),1),
    		 yAxe(QColor("yellow"),1),
    		 zAxe(QColor("yellow"),1),
    		 xHideAxe(QColor("yellow"),1,DotLine),
    		 yHideAxe(QColor("yellow"),1,DotLine),
    		 zHideAxe(QColor("yellow"),1,DotLine),
    		 text2Axe(QColor("white"),3),
    		 text1Axe(QColor("darkgreen"),3);

    //x0 = min(max(x0,0),n1);
    //y0 = min(max(y0,0),n2);

    double xOrder = 1.0, yOrder = 1.0;

    if ( cphi < 0 )	{
    	swap( y1, y2 );
    	yOrder *= -1;
    	//swap( x1, x2 );
    }		

    if ( sphi < 0 ) {
    	swap( x1, x2 );
    	xOrder *= -1;
    }


    double hx = (x2-x1)/n1;
    double hy = (y2-y1)/n2;
     
    Vector Edge1, Edge2, n;
    Point2 facet[4];
    int  i,j;
     
    if ( e2.z>=0 ) {
       ymax += e2.z/2;
       ymin -= e2.z/2;
    }   
    else {
       ymin += e2.z/2;
       ymax -= e2.z/2;
    }
     
    double ax = -sizex*xmin/(xmax-xmin);
    double bx = sizex/(xmax-xmin); 
    double ay = -sizey*ymin/(ymax-ymin);
    double by = -sizey/(ymax-ymin);
     
    for (i=0; i<n1; i++) 
    {
        CurPoint[i].x = x1+i*hx;
        CurPoint[i].y = y1;
        if ( table == NULL ) {
        try {
            CurPoint[i].z = f(CurPoint[i].x,CurPoint[i].y);
            CurLine[i].x = int(ax+bx*( CurPoint[i] & e1 ));
            CurLine[i].y = int(ay+by*( CurPoint[i] & e2 ));
        } catch ( RangeException e ) { CurPoint[i].z = NAN; }
        } else {              // use previously calculated table
                CurPoint[i].z = table[ translInd(i,0) ];
                if ( CurPoint[i].z != NAN ) {
                 CurLine[i].x = int(ax+bx*( CurPoint[i] & e1 ));
	               CurLine[i].y = int(ay+by*( CurPoint[i] & e2 ));
	              }
        }
    }
    emit startPaint();
    for (i=1; i<n2; i++)
    {
        if ( (i*20)%n2 == 0 && ( table == NULL ) )
        {
            emit done_perc( i*100/n2 );
        }
//        if (!painting) break;
        for (j=0; j<n1; j++)
        {
            NextPoint[j].x = x1+j*hx;
            NextPoint[j].y = y1+i*hy;
            if ( table == NULL ) {
            try {
                NextPoint[j].z = f( NextPoint[j].x, NextPoint[j].y );
                NextLine[j].x = int(ax+bx*( NextPoint[j] & e1 ));
                NextLine[j].y = int(ay+by*( NextPoint[j] & e2 ));
            } catch ( RangeException e ) { NextPoint[j].z = NAN; }
            } else {              // use previously calculated table
                NextPoint[j].z = table[ translInd(j,i)/*j + i*n1*/ ];
                if ( NextPoint[j].z != NAN ) {
                	NextLine[j].x = int(ax+bx*( NextPoint[j] & e1 ));
	              	NextLine[j].y = int(ay+by*( NextPoint[j] & e2 ));
                }
            }

        }

        p->setPen( NoPen );

        for (j=0; j<n1-1; j++)
        {
            // first triangle
            if ( CurPoint[j].z==NAN || CurPoint[j+1].z==NAN ||
	           NextPoint[j].z==NAN || NextPoint[j+1].z==NAN  ) continue;  // Range exception
	
                Edge1 = ( CurPoint[j+1] - CurPoint[j] ) * xOrder;
                Edge2 = ( NextPoint[j] - CurPoint[j] ) * yOrder;
                n = Edge1 ^ Edge2;
                double ne3 = (n & e3);

                if ( ne3 > 0)
                	*pcolor = cDown.light(int(45-contrast/2+(contrast+60)*ne3 / !n ));
                else
                	*pcolor = cUp.light(int(45-contrast/2-(contrast+60)*ne3/!n));	

                p->setBrush(*pcolor);

                 
                facet[0] = CurLine[j];
                facet[1] = CurLine[j+1];
//                facet[2] = NextLine[j+1];
                facet[2] = NextLine[j];
                 
                parr->setPoints(3, facet[0].x, facet[0].y, facet[1].x, facet[1].y,
                									 facet[2].x, facet[2].y/*, facet[3].x, facet[3].y*/) ;
                p->drawPolygon(*parr);

                // Second triangle
                 
                Edge1 = ( NextPoint[j+1] - CurPoint[j+1] ) * yOrder;
                //Edge2 = ( NextPoint[j]-CurPoint[j+1] ) * xOrder * yOrder;
                Edge2 = ( NextPoint[j] - NextPoint[j+1] ) * xOrder;
                n = Edge1 ^ Edge2;
                ne3 = (n & e3);

                if (ne3 > 0)
                	*pcolor = cDown.light(int( 45-contrast/2+(60+contrast) * ne3 / !n ));
                else
                	*pcolor = cUp.light(int( 45-contrast/2-(60+contrast) * ne3 / !n ));	

                p->setBrush(*pcolor);

                facet[0] = CurLine[j+1];
                facet[1] = NextLine[j];
                facet[2] = NextLine[j+1];
                 
                 
                parr->setPoints(3, facet[0].x, facet[0].y, facet[1].x, 
                                facet[1].y,facet[2].x, facet[2].y ) ; 
                p->drawPolygon(*parr);

                if ( table == NULL && axesData.useGrid ) {
	                if ( j/5*5==j ) {
  	              	p->setPen( pcolor->light() );
    	            	p->drawLine(CurLine[j].x,CurLine[j].y,NextLine[j].x,NextLine[j].y);
      	          }
        	        if ( i/5*5==i ) {
          	      	p->setPen( pcolor->light() );
            	    	p->drawLine(CurLine[j].x,CurLine[j].y,CurLine[j+1].x,CurLine[j+1].y);
              	  }
                }


                if ( axesData.addAxes ) {
                if ( i == y0 ) {
				         	p->setPen(xAxe);
        				 	int ax1 =  int(ax+bx*CurPoint[j].x*e1.x);
				         	int ay1 =  int(ay+by*CurPoint[j].x*e2.x);
				         	int ax2 =  int(ax+bx*CurPoint[j+1].x*e1.x);
				         	int ay2 =  int(ay+by*CurPoint[j+1].x*e2.x);
				         	p->drawLine( ax1,ay1,ax2,ay2 );
				         					         				         	
  					    }
  					
  					    if ( j == x0 ) {
				         	p->setPen(yAxe);
        				 	int ax1 =  int(ax+bx*CurPoint[j].y*e1.y);
				         	int ay1 =  int(ay+by*CurPoint[j].y*e2.y);
				         	int ax2 =  int(ax+bx*NextPoint[j].y*e1.y);
				         	int ay2 =  int(ay+by*NextPoint[j].y*e2.y);
				         	p->drawLine( ax1,ay1,ax2,ay2 );
				        }
  					
  					    if ( j == x0 && i == y0 ) {
				         	p->setPen(zAxe);
        				 	int ax1 =  int(ax+bx*e1.z);
				         	int ay1 =  int(ay+by*e2.z);
				         	int ax2 =  int(ax-bx*e1.z);
				         	int ay2 =  int(ay-by*e2.z);
				         	p->drawLine( ax1,ay1,ax2,ay2 );
					       } 	
  					    }
  					
                p->setPen( NoPen );


      }

         
      for ( j = 0; j<n1; j++ )
    	{
        	CurLine[j] = NextLine [j];
        	CurPoint[j] = NextPoint [j];
      }
    }


    if ( axesData.addAxes ) { // Draw axes (Dots)
    	p->setFont( QFont( "courier", 10, 70) );
    	
			p->setPen(xHideAxe);
		 	int ax1 =  int(ax+bx*x1*e1.x);
	 	 	int ay1 =  int(ay+by*x1*e2.x);
 			int ax2 =  int(ax+bx*x2*e1.x);
	   	int ay2 =  int(ay+by*x2*e2.x);
	 	 	p->drawLine( ax1,ay1,ax2,ay2 );
	 	 	p->setPen(text1Axe);
 		 	p->drawText(ax2-1,ay2-1,"X");
 		 	p->setPen(text2Axe);
 		 	p->drawText(ax2,ay2,"X");
 	 	
	 	 	p->setPen(yHideAxe);
		 	ax1 =  int(ax+bx*y1*e1.y);
 	 		ay1 =  int(ay+by*y1*e2.y);
	 		ax2 =  int(ax+bx*y2*e1.y);
  	 	ay2 =  int(ay+by*y2*e2.y);
 	 		p->drawLine( ax1,ay1,ax2,ay2 );
 	 		p->setPen(text1Axe);
 		 	p->drawText(ax2-1,ay2-1,"Y");
	 	 	p->setPen(text2Axe);
 		 	p->drawText(ax2,ay2,"Y");

 	 	
	 	 	p->setPen(zHideAxe);
		 	ax1 =  int(ax-bx*e1.z);
 	 		ay1 =  int(ay-by*e2.z);
	 		ax2 =  int(ax+bx*e1.z);
  	 	ay2 =  int(ay+by*e2.z);
 	 		p->drawLine( ax1,ay1,ax2,ay2 );
	 	 	p->setPen(text1Axe);
 		 	p->drawText(ax2-1,ay2-1,"Z");
 		 	p->setPen(text2Axe);
 		 	p->drawText(ax2,ay2,"Z");
 		}
 	 	
 		if ( axesData.showPoints ) {
 	 		double x = 0, y = 0, z = 0;
	 	 	p->setPen(QColor("red"));
 		 	p->setBrush(QColor("white"));
 		 	p->setFont( QFont( "helvetica", 8,25 ) );
 		 	
 		 	char num[21];
 		 	
 		 	double axeStep = axesData.pointsStep;
 	 	  int px,py;
	 	 	for (x = axeStep; x < max(x1,x2); x+=axeStep ) {
	 	 	  if ( !inside(x,x1,x2) ) continue;
	 	 		px = int(ax+bx*x*e1.x)-1;
	 	 		py = int(ay+by*x*e2.x)-1;
  	 		p->drawRect(px,py,3,3);
  	 		if (axesData.showNumbers ) {
  	 			snprintf(num,20,"%g",x);
  	 			p->setPen(QColor("white"));
	  	 		p->drawText(px,py+14,num);
	  	 		p->setPen(QColor("red"));
	  	 	}
  	 	}
   		for (x = -axeStep; x > min(x1,x2); x-=axeStep ) {
   			if ( !inside(x,x1,x2) ) continue;
   			px = int(ax+bx*x*e1.x)-1;
	 	 		py = int(ay+by*x*e2.x)-1;
   			p->drawRect(px,py,3,3);	
   			
  	 		if (axesData.showNumbers) {
  	 			snprintf(num,20,"%g",x);
  	 			p->setPen(QColor("white"));
	  	 		p->drawText(px,py+14,num);
	  	 		p->setPen(QColor("red"));
	  	 	}
   		}
 		 	for (y = axeStep; y < max(y1,y2); y+=axeStep ) {
 		 		if ( !inside(y,y1,y2) ) continue;
 		 		px = int(ax+bx*y*e1.y)-1;
 		 		py = int(ay+by*y*e2.y)-1;
   			p->drawRect(px,py,3,3);
  	 		if (axesData.showNumbers) {
  	 			snprintf(num,20,"%g",y);
  	 			p->setPen(QColor("white"));
	  	 		p->drawText(px,py+14,num);
	  	 		p->setPen(QColor("red"));
	  	 	}
   		}

 		 	for (y = -axeStep; y > min(y1,y2); y-=axeStep ) {
 		 		if ( !inside(y,y1,y2) ) continue;
 		 		px = int(ax+bx*y*e1.y)-1;
 		 		py = int(ay+by*y*e2.y)-1;
   			p->drawRect(px,py,3,3);
  	 		if (axesData.showNumbers) {
  	 			snprintf(num,20,"%g",y);
  	 			p->setPen(QColor("white"));
	  	 		p->drawText(px,py+14,num);
	  	 		p->setPen(QColor("red"));
	  	 	}
   		}
   		
   		
   		for (z = 0; z < fmax*1.8; z+=max(axeStep, (fmax-fmin)/4) ) {
   			if ( !inside(z,2*fmin-fmax,2*fmax-fmin) ) continue;
   			px = int(ax+bx*((z-fmin)/(fmax-fmin)*0.8-0.4)*e1.z)-1;
 		 		py = int(ay+by*((z-fmin)/(fmax-fmin)*0.8-0.4)*e2.z)-1;
   			p->drawRect(px,py,3,3);
  	 		if (axesData.showNumbers) {
  	 			snprintf(num,20,"%g",z);
  	 			p->setPen(QColor("white"));
	  	 		p->drawText(px,py+14,num);
	  	 		p->setPen(QColor("red"));
	  	 	}
   		}

			for (z = 0; z > fmin*1.8; z-=max(axeStep, (fmax-fmin)/4) ) {
				if ( !inside(z,2*fmin-fmax,2*fmax-fmin) ) continue;
 		 		px = int(ax+bx*((z-fmin)/(fmax-fmin)*0.8-0.4)*e1.z)-1;
 		 		py = int(ay+by*((z-fmin)/(fmax-fmin)*0.8-0.4)*e2.z)-1;
   			p->drawRect(px,py,3,3);
  	 		if (axesData.showNumbers) {
  	 			snprintf(num,20,"%g",z);
  	 			p->setPen(QColor("white"));
	  	 		p->drawText(px,py+14,num);
	  	 		p->setPen(QColor("red"));
	  	 	}
   		}   		
   		
   		
   	}	   		
 	 		
    if ( table == NULL ) {
 	   	emit done_perc(100);
    	emit done_perc(0);
    }	
    delete CurLine;
    delete NextLine;
    delete CurPoint;
    delete NextPoint;
    painting = false;
    x1 = old_x1;
    x2 = old_x2;
    y1 = old_y1;
    y2 = old_y2;
}

void PlotField::mouseMoveEvent(QMouseEvent *ev)
{
    if (painting) return;
    if (!mousePressed) return;
    QPoint delta = ev->pos()-currentPos;
    currentPos = ev->pos();
    
    phi += double(delta.x())/width()*2*PI;
    while ( phi > 2*PI ) phi-=2*PI;
    while ( phi < 0 ) phi+=2*PI;
    
    psi += double(delta.y())/height()*2*PI;
    if ( psi > PI/2 ) psi = PI/2;
    if ( psi < -PI/2 ) psi = -PI/2;
    
    char mesg[128];
    snprintf(mesg,128,"new coord : Phi = %i ;  Psi = %i", int(phi/PI*180), int(psi/PI*180)); 
    emit showStatus(mesg);
    if ( surfaceType == shaded ) {
	    int oldn1 = n1;
  	  int oldn2 = n2;
    	n1 = rot_n1;
	    n2 = rot_n2;
	    PrepareImage(rot_tab);
	    repaint(false);
	    n1 = oldn1;
  	  n2 = oldn2;
  	} else {
  		PrepareImage();
  		repaint(false);
  	}	
}

void PlotField::mousePressEvent(QMouseEvent *ev)
{
    currentPos = ev->pos();
    mousePressed = true;
    if ( surfaceType == shaded ) {
	    rot_tab = new double [ (rot_n1+1)*(rot_n2+1) ];
	    double hx = (x2-x1)/rot_n1;
  	  double hy = (y2-y1)/rot_n2;
    	
	    for (int i=0; i<rot_n2; i++)
  	      for (int j=0; j<rot_n1; j++)
    	        try {
      	           rot_tab[i*rot_n1+j] = f(  x1+j*hx,  y1+i*hy );
        	    }
          	  catch ( RangeException )  {
            	     rot_tab[ i*rot_n1+j ] = NAN;
	            }
	  }
}

void PlotField::mouseReleaseEvent(QMouseEvent *)
{
    mousePressed = false;
    Paint();
    if ( surfaceType == shaded )
    	delete rot_tab;
}

















