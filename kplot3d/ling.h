/***************************************************************************
                          ling.h  -  description                              
                             -------------------                                         
    begin                : Thu Apr 1 1999                                           
    copyright            : (C) 1999 by Dmitry Poplavsky                         
    email                : pdv@hpserv.rpd.univ.kiev.ua                                     
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   * 
 *                                                                         *
 ***************************************************************************/


#ifndef LING_H
#define LING_H

#ifndef PI
#define PI 3.14159265
#endif
#include <string.h>
#include <getdata.h>

/**       parser
  *@author Dmitry Poplavsky
  */

class Exception {
        public:
        char s[128];
        int pos;
        Exception () { strcpy(s,"Exception..."); }
        Exception ( char *ns , int p = 0) : pos(p)
        		{ strncpy(s, ns, 127); }
        ~Exception () {}
};

class SyntaxException : public Exception {
        public:
        SyntaxException() { strcpy(s,"Syntax exception..."); }
        SyntaxException( char * ns , int p = 0 ) : Exception(ns,p) {}
        ~SyntaxException () {}
};


class RangeException : public Exception {
        public:
        RangeException() { strcpy(s,"Range exception..."); }
        RangeException( char* ns , int p = 0 ) : Exception(ns,p)  {}
        ~RangeException () {}
};


class Ling {
private:
   int pos;
   GetData getData;
   char s[1024];
   double x,y;
   char nextchar();
   double expr();
   double mul();
   double num();
//   int isDelim(char);
public:
    Ling(char * s);
    Ling();
    ~Ling();
    double calc(double x = 0, double y = 0);
    void setFunc ( const  char * s );
};

#endif





