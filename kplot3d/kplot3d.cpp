/***************************************************************************
                          kplot3d.cpp  -  description                              
                             -------------------                                         
    begin                : ��� ��� 11 15:22:15 EEST 1999
                                           
    copyright            : (C) 1999 by Dmitry Poplavsky                         
    email                : dima@linuxfan.com                                     
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   * 
 *                                                                         *
 ***************************************************************************/


#include <kplot3d.h>
#include "DataRanges.h"
#include "toggle.xpm"
#include "axes.xpm"
#include <math.h>


Kplot3dApp::Kplot3dApp()
{
  setCaption("Kplot3d " VERSION);


  ///////////////////////////////////////////////////////////////////
  // call inits to invoke all other construction parts
  initMenuBar();
  initToolBar();
  initStatusBar();
  initView();

  ///////////////////////////////////////////////////////////////////
  // read the config file options
  readOptions();

  ///////////////////////////////////////////////////////////////////
  // enable bars dependend on config file setups
  if (!bViewToolbar)
    enableToolBar(KToolBar::Hide,0);
  if (!bViewStatusbar)
    enableStatusBar(KStatusBar::Hide);

  menuBar()->setMenuBarPos(menu_bar_pos);
  toolBar()->setBarPos(tool_bar_pos);

  connect(view->plotField,SIGNAL(showStatus(const char*)), this, SLOT(slotStatusMsg(const char*)));
  connect(view,SIGNAL(showStatus(const char*)), this, SLOT(slotStatusMsg(const char*)));
  connect(view->plotField,SIGNAL(done_perc(int)), progress, SLOT(setValue(int)));


  ///////////////////////////////////////////////////////////////////
  // disable menu and toolbar items at startup

  resize(470,370);

}

Kplot3dApp::~Kplot3dApp()
{
  
}

void Kplot3dApp::enableCommand(int id_)
{
  ///////////////////////////////////////////////////////////////////
  // enable menu and toolbar functions by their ID's
  menuBar()->setItemEnabled(id_,true);
  toolBar()->setItemEnabled(id_,true);
}

void Kplot3dApp::disableCommand(int id_)
{
  ///////////////////////////////////////////////////////////////////
  // disable menu and toolbar functions by their ID's
  menuBar()->setItemEnabled(id_,false);
  toolBar()->setItemEnabled(id_,false);
}


void Kplot3dApp::initMenuBar()
{

  ///////////////////////////////////////////////////////////////////
  // MENUBAR  

  ///////////////////////////////////////////////////////////////////
  // menuBar entry file_menu
  file_menu = new QPopupMenu();
  file_menu->insertItem(Icon("mini/kplot3dapp.xpm"), i18n("New &Window"), ID_FILE_NEW_WINDOW );
//  file_menu->insertSeparator();
/*  file_menu->insertItem(Icon("filenew.xpm"), i18n("&New"), ID_FILE_NEW );*/
//  file_menu->insertItem(Icon("fileopen.xpm"), i18n("&Open..."), ID_FILE_OPEN );
  file_menu->insertSeparator();
  file_menu->insertItem(Icon("filefloppy.xpm") ,i18n("&Save image ..."), ID_FILE_SAVE_IMAGE );
  file_menu->insertItem(Icon("filefloppy.xpm") ,i18n("&Save data ..."), ID_FILE_SAVE_DATA );
//  file_menu->insertItem(i18n("Save &as"), ID_FILE_SAVE_AS );
//  file_menu->insertItem(i18n("&Close"), ID_FILE_CLOSE );
//  file_menu->insertSeparator();
  file_menu->insertItem(Icon("fileprint.xpm"), i18n("&Print"), ID_FILE_PRINT );
  file_menu->insertSeparator();
  file_menu->insertItem(i18n("C&lose Widow"), ID_FILE_CLOSE_WINDOW);
  file_menu->insertSeparator();
  file_menu->insertItem(i18n("E&xit"), ID_FILE_QUIT );

  // file_menu key accelerators
  file_menu->setAccel(CTRL+Key_O, ID_FILE_OPEN);
  file_menu->setAccel(CTRL+Key_S, ID_FILE_SAVE_IMAGE);
  file_menu->setAccel(CTRL+Key_A, ID_FILE_SAVE_AS);
  file_menu->setAccel(CTRL+Key_W, ID_FILE_CLOSE);
  file_menu->setAccel(CTRL+Key_P, ID_FILE_PRINT);
  file_menu->setAccel(CTRL+Key_Q, ID_FILE_QUIT);



  ///////////////////////////////////////////////////////////////////
  // menuBar entry options_menu
  options_menu = new QPopupMenu();
  options_menu->insertItem(i18n("&Ranges"), ID_RANGES);
  options_menu->insertItem(i18n("&Colors"), ID_COLORS );
  options_menu->insertSeparator();
  options_menu->insertItem(i18n("&Axes"), ID_GENERAL);
  options_menu->insertSeparator();
  options_menu->insertItem(i18n("&Toggle surface type"), ID_TOGGLE);


  ///////////////////////////////////////////////////////////////////
  // EDIT YOUR APPLICATION SPECIFIC MENUENTRIES HERE
  

  ///////////////////////////////////////////////////////////////////
  // menuBar entry help_menu
  help_menu = new QPopupMenu();
  help_menu = kapp->getHelpMenu(true, i18n(IDS_APP_ABOUT));


  ///////////////////////////////////////////////////////////////////
  // MENUBAR CONFIGURATION
  // set menuBar() the current menuBar and the position due to config file
  menuBar()->insertItem(i18n("&File"), file_menu);
//  menuBar()->insertItem(i18n("&Edit"), edit_menu);
  menuBar()->insertItem(i18n("&Options"), options_menu);

  ///////////////////////////////////////////////////////////////////
  // INSERT YOUR APPLICATION SPECIFIC MENUENTRIES HERE


  menuBar()->insertSeparator();
  menuBar()->insertItem(i18n("&Help"), help_menu);

  ///////////////////////////////////////////////////////////////////
  // CONNECT THE SUBMENU SLOTS WITH SIGNALS

  CONNECT_CMD(file_menu);
  CONNECT_CMD(options_menu);

}
void Kplot3dApp::initToolBar()
{

  ///////////////////////////////////////////////////////////////////
  // TOOLBAR
  // set toolBar() the current toolBar and the position due to config file
  toolBar()->insertButton(Icon("filenew.xpm"), ID_FILE_NEW_WINDOW, true, i18n("New window"));
//  toolBar()->insertButton(Icon("fileopen.xpm"), ID_FILE_OPEN, true, i18n("Open File"));
  toolBar()->insertButton(Icon("filefloppy.xpm"), ID_FILE_SAVE_IMAGE, true, i18n("Save image as..."));
  toolBar()->insertButton(Icon("fileprint.xpm"), ID_FILE_PRINT, true, i18n("Print"));
  toolBar()->insertSeparator();
  toolBar()->insertButton(Icon("move.xpm"), ID_RANGES,true,i18n("Set ranges"));
  toolBar()->insertButton(Icon("paintbrush.xpm"), ID_COLORS,true,i18n("Set colors"));
//  toolBar()->insertButton(Icon("editcut.xpm"), ID_EDIT_CUT, true, i18n("Cut"));
//  toolBar()->insertButton(Icon("editcopy.xpm"), ID_EDIT_COPY, true, i18n("Copy"));
//  toolBar()->insertButton(Icon("editpaste.xpm"), ID_EDIT_PASTE, true, i18n("Paste"));
//  toolBar()->insertSeparator();

	toolBar()->insertButton( QPixmap( axes ), ID_GENERAL, true, i18n("Axes options"));
  toolBar()->insertButton( QPixmap( toggle_icon ), ID_TOGGLE, true, i18n("Toggle surface type"));
  toolBar()->insertButton(Icon("help.xpm"), ID_HELP, SIGNAL(pressed()), kapp, SLOT(appHelpActivated()), true, i18n("Help"));

  ///////////////////////////////////////////////////////////////////
  // INSERT YOUR APPLICATION SPECIFIC TOOLBARS HERE -e.g. tool_bar_1:
  // add functionality for new created toolbars in:
  // enableCommand, disableCommand, in the menuBar() and an additional function slotViewToolbar_1
  // for that also create a bViewToolbar_1 and a KConfig entry (see Constructor).
  // Also update resource values and commands 


  ///////////////////////////////////////////////////////////////////
  // CONNECT THE TOOLBAR SLOTS WITH SIGNALS - add new created toolbars
  CONNECT_TOOLBAR(toolBar());

}

void Kplot3dApp::initStatusBar()
{
  ///////////////////////////////////////////////////////////////////
  //STATUSBAR
  progress = new KProgress( statusBar(), "progress");
  progress->setBarStyle(KProgress::Blocked);
  // progress->setBarColor(QColor(20,20,160));
  // space before progress :
  statusBar()->insertWidget(new QWidget( statusBar() ) , 4, ID_STATUS_SPACE );
  statusBar()->insertWidget(progress , 80, ID_STATUS_PROGRESS );
  // space after progress :
  statusBar()->insertWidget(new QWidget( statusBar() ) , 10, ID_STATUS_SPACE );
  statusBar()->insertItem(IDS_DEFAULT, ID_STATUS_MSG );
  statusBar()->setInsertOrder(KStatusBar::LeftToRight);

}

void Kplot3dApp::initView()
{ 
//  doc = new Kplot3dDoc(this);

  ////////////////////////////////////////////////////////////////////
  // set the main widget here
  KApplication *app=KApplication::getKApplication();
  view = new Kplot3dView(app,this);
  setView(view);
}

bool Kplot3dApp::queryExit()
{
	return true;
  int exit=KMsgBox::yesNo(this, i18n("Exit"), i18n("Really Quit ?"));

  if(exit==1)
    return true;
  else
    return false;
}

void Kplot3dApp::saveOptions()
{
  KConfig *config = kapp->getConfig();

  config->setGroup("APPEARANCE");
  config->writeEntry("Version",VERSION);
  config->writeEntry("ShowToolbar",toolBar()->isVisible());
  config->writeEntry("ShowStatusbar",statusBar()->isVisible());
  config->writeEntry("MenuBarPos", (int)menuBar()->menuBarPos());
  config->writeEntry("ToolBar_Pos", (int)toolBar()->barPos());

  config->setGroup("RANGES");
  config->writeEntry("min_x",view->data().x1);
  config->writeEntry("max_x",view->data().x2);
  config->writeEntry("min_y",view->data().y1 );
  config->writeEntry("max_y",view->data().y2);
  config->writeEntry("n_x",view->data().surf_nx);
  config->writeEntry("n_y",view->data().surf_ny);
  config->writeEntry("rot_nx",view->data().rot_nx);
  config->writeEntry("rot_ny",view->data().rot_ny);
  config->writeEntry("line_nx",view->data().line_nx);
  config->writeEntry("line_ny",view->data().line_ny);
  config->writeEntry("phi",view->data().phi);
  config->writeEntry("psi",view->data().psi);

  config->setGroup("COLORS");
  config->writeEntry("Up_color",view->plotField->cUp);
  config->writeEntry("Down_color",view->plotField->cDown);
  config->writeEntry("Up_color_line",view->plotField->cLineUp);
  config->writeEntry("Down_color_line",view->plotField->cLineDown);
  config->writeEntry("surface_type",(int)view->plotField->surfaceType);
  config->writeEntry("contrast",view->plotField->contrast);

  AxesData adata = view->plotField->axesData;
  config->setGroup("AXES");
  config->writeEntry( "useGrid", adata.useGrid);
  config->writeEntry( "gridStep", adata.gridStep );
  config->writeEntry( "addAxes", adata.addAxes );
  config->writeEntry( "moveCenter", adata.moveCenter );
  config->writeEntry( "showPoints", adata.showPoints );
  config->writeEntry( "showNumbers", adata.showNumbers );
  config->writeEntry( "pointsStep", adata.pointsStep );
  config->writeEntry( "showBox", adata.showBox );
  config->writeEntry( "boxColor", adata.boxColor );

 }

void Kplot3dApp::readOptions()
{
  ///////////////////////////////////////////////////////////////////
  // read the config file entries
  KConfig *config = kapp->getConfig();
  DataRanges data;
  double version;

	config->setGroup("APPEARANCE");
  bViewToolbar = config->readBoolEntry("ShowToolbar", true);
  bViewStatusbar = config->readBoolEntry("ShowStatusbar", true);
  menu_bar_pos = (KMenuBar::menuPosition)config->readNumEntry("MenuBarPos", KMenuBar::Top);
  tool_bar_pos = (KToolBar::BarPosition)config->readNumEntry("ToolBar_Pos", KToolBar::Top);
  version = config->readDoubleNumEntry("Version", 0.0);

  config->setGroup("RANGES");
  data.x1 = config->readDoubleNumEntry("min_x", -3.0);
  data.x2 = config->readDoubleNumEntry("max_x", 3.0);
  data.y1 = config->readDoubleNumEntry("min_y", -3.0);
  data.y2 = config->readDoubleNumEntry("max_y", 3.0);
  if (version < 0.605) {
  	data.x1 = -3;
  	data.x2 =  3;
  	data.y1 = -3;
  	data.y2 =  3;
  }
  data.surf_nx = config->readNumEntry("n_x", 100);
  data.surf_ny = config->readNumEntry("n_y", 100);
  data.rot_nx = config->readNumEntry("rot_nx", 30);
  data.rot_ny = config->readNumEntry("rot_ny", 30);
  data.line_nx = config->readNumEntry("line_nx", 50);
  data.line_ny = config->readNumEntry("line_ny", 50);
  data.phi = config->readDoubleNumEntry("phi", 30.0*M_PI/180);
  data.psi = config->readDoubleNumEntry("psi", 20.0*M_PI/180);

  view->setData(data);

	config->setGroup("COLORS");
	QColor cUp = QColor(0,0,200);
	QColor cDown = QColor(128,128,128);
	QColor cLineUp = QColor(80,80,80);
	QColor cLineDown = QColor(0,160,0);

  view->plotField->cUp = config->readColorEntry("Up_color",&cUp);
  view->plotField->cDown = config->readColorEntry("Down_color",&cDown);
  view->plotField->cLineUp = config->readColorEntry("Up_color_line",&cLineUp);
  view->plotField->cLineDown = config->readColorEntry("Down_color_line",&cLineDown);
  view->plotField->surfaceType = (PlotField::SurfaceType)config->readNumEntry("surface_type",(int)PlotField::shaded);
  view->plotField->contrast = config->readNumEntry("contrast",50);

  AxesData adata;
  config->setGroup("AXES");
  adata.useGrid = config->readBoolEntry("useGrid",true);
  adata.gridStep = config->readDoubleNumEntry("GridStep",1.0);
  adata.addAxes = config->readBoolEntry("addAxes",false);
  adata.moveCenter = config->readBoolEntry("moveCenter",false);
  adata.showPoints = config->readBoolEntry("showPoints",false);
  adata.showNumbers = config->readBoolEntry("showNumbers",false);
  adata.showBox = config->readBoolEntry("showBox",false);
  adata.pointsStep = config->readDoubleNumEntry("pointsStep",1.0);
  QColor boxColor(0,0,100);
  adata.boxColor = config->readColorEntry("boxColor",&boxColor);

  view->plotField->axesData = adata;

}


/////////////////////////////////////////////////////////////////////
// SLOT IMPLEMENTATION
/////////////////////////////////////////////////////////////////////

void Kplot3dApp::slotFileNewWindow()
{
  slotStatusMsg(i18n("Opening a new Application window..."));
  (new Kplot3dApp)->show();
  slotStatusMsg(IDS_DEFAULT);
}


void Kplot3dApp::slotFileOpen()
{
  slotStatusMsg(i18n("Opening file..."));

  slotStatusMsg(IDS_DEFAULT);
}



void Kplot3dApp::slotFileSaveAs()
{
  slotStatusMsg(i18n("Saving file under new filename..."));

  slotStatusMsg(IDS_DEFAULT);
}

void Kplot3dApp::slotFileSaveImage()
{
  slotStatusMsg(i18n("Saving image of function..."));
  view->saveImg();

  slotStatusMsg(IDS_DEFAULT);
}

void Kplot3dApp::slotFileSaveData()
{
  slotStatusMsg(i18n("Saving data in x y z format ..."));
  view->saveData();

  slotStatusMsg(IDS_DEFAULT);
}


void Kplot3dApp::slotFileClose()
{
  slotStatusMsg(i18n("Closing file..."));

  slotStatusMsg(IDS_DEFAULT);
}

void Kplot3dApp::slotFilePrint()
{
  slotStatusMsg(i18n("Printing..."));

  QPrinter printer;
  if (printer.setup(this))
    {
      QPainter painter;
      painter.begin( &printer );

      ///////////////////////////////////////////////////////////////////
      // TODO: Define printing by using the QPainter methods here

      painter.drawPixmap( 1, 1, *(view->plotField->getPix()) );

      painter.end();
    };

  slotStatusMsg(IDS_DEFAULT);
}

void Kplot3dApp::slotFileCloseWindow()
{
  close();
}

void Kplot3dApp::slotFileQuit()
{ 

  ///////////////////////////////////////////////////////////////////
  // exits the Application
  if(this->queryExit())
    {
      saveOptions();
      KTMainWindow::deleteAll();
      kapp->quit();
   }
  else
    slotStatusMsg(IDS_DEFAULT);
    return;
}


void Kplot3dApp::slotStatusMsg(const char *text)
{
  ///////////////////////////////////////////////////////////////////
  // change status message permanently
  // statusBar()->clear();
  statusBar()->changeItem(text, ID_STATUS_MSG );
}


void Kplot3dApp::slotStatusHelpMsg(const char *text)
{
  ///////////////////////////////////////////////////////////////////
  // change status message of whole statusbar temporary (text, msec)
  statusBar()->changeItem(text, ID_STATUS_MSG );
  // statusBar()->message(text, 2000);
}


void Kplot3dApp::slotColors()
{
    slotStatusMsg(i18n("set colors of image..."));
	view->slotSetColor();
}

void Kplot3dApp::slotToggle()
{
  slotStatusMsg(i18n("Toggle surface type ..."));
	view->plotField->toggleSurfaceType();
}


void Kplot3dApp::slotOptionsGeneral()
{
  slotStatusMsg(i18n("axes settings ..."));
	view->slotOptionsAxes();
}




void Kplot3dApp::slotRanges()
{
	slotStatusMsg(i18n("Set ranges for x,y , and rotate view point..."));
	view->slotOptions();
}



void Kplot3dApp::commandCallback(int id_){
  switch (id_){
    ON_CMD(ID_FILE_NEW_WINDOW,          slotFileNewWindow())
    ON_CMD(ID_FILE_OPEN,                slotFileOpen())
    ON_CMD(ID_FILE_SAVE_IMAGE,                slotFileSaveImage())
    ON_CMD(ID_FILE_SAVE_DATA,                slotFileSaveData())
    ON_CMD(ID_FILE_SAVE_AS,             slotFileSaveAs())
    ON_CMD(ID_FILE_CLOSE,               slotFileClose())
    ON_CMD(ID_FILE_PRINT,               slotFilePrint())
    ON_CMD(ID_FILE_CLOSE_WINDOW,        slotFileCloseWindow())
    ON_CMD(ID_FILE_QUIT,                slotFileQuit())

    ON_CMD(ID_RANGES,             slotRanges())
    ON_CMD(ID_COLORS,           slotColors())
    ON_CMD(ID_TOGGLE,           slotToggle())
    ON_CMD(ID_GENERAL,           slotOptionsGeneral())
  }
}

void Kplot3dApp::statusCallback(int id_){
  switch (id_){
    ON_STATUS_MSG(ID_FILE_NEW_WINDOW,   i18n("Opens a new application window"))
    ON_STATUS_MSG(ID_FILE_NEW,          i18n("Creates a new document"))
//    ON_STATUS_MSG(ID_FILE_OPEN,         i18n("Opens an existing document"))
//    ON_STATUS_MSG(ID_FILE_SAVE,         i18n("Save the actual document"))
    ON_STATUS_MSG(ID_FILE_SAVE_IMAGE,      i18n("Save image..."))
    ON_STATUS_MSG(ID_FILE_CLOSE,        i18n("Closes the actual file"))
    ON_STATUS_MSG(ID_FILE_PRINT,        i18n("Prints the current document"))
    ON_STATUS_MSG(ID_FILE_CLOSE_WINDOW, i18n("Closes the current window"))
    ON_STATUS_MSG(ID_FILE_QUIT,         i18n("Exits the program"))
    ON_STATUS_MSG(ID_RANGES,         i18n("Set ranges for x,y"))
    ON_STATUS_MSG(ID_COLORS,         i18n("Set colors"))


//    ON_STATUS_MSG(ID_EDIT_CUT,          i18n("Cuts the selected section and puts it to the clipboard"))
//    ON_STATUS_MSG(ID_EDIT_COPY,         i18n("Copys the selected section to the clipboard"))
//    ON_STATUS_MSG(ID_EDIT_PASTE,        i18n("Pastes the clipboard contents to actual position"))
//    ON_STATUS_MSG(ID_EDIT_SELECT_ALL,   i18n("Selects the whole document contents"))


//    ON_STATUS_MSG(ID_VIEW_TOOLBAR,      i18n("Enables / disables the actual Toolbar"))
//    ON_STATUS_MSG(ID_VIEW_STATUSBAR,    i18n("Enables / disables the Statusbar"))
  }
}

























































