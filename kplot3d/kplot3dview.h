/***************************************************************************
                          kplot3dview.h  -  description                              
                             -------------------                                         
    begin                : ��� ��� 11 15:22:15 EEST 1999
                                           
    copyright            : (C) 1999 by Dmitry Poplavsky                         
    email                : dima@linuxfan.com                                     
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   * 
 *                                                                         *
 ***************************************************************************/


#ifndef KPLOT3DVIEW_H
#define KPLOT3DVIEW_H
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif 

#include <kapp.h>
#include <qwidget.h>
#include <qpushbutton.h>
#include <qfont.h>
#include <qlineedit.h>
#include "PlotField.h"
#include "OptionsDialog.h"
#include "DataRanges.h"
#include "qcombobox.h"


/**
  *
   */

class Kplot3dView : public QWidget
{
  Q_OBJECT

 public:
  /** Constructor for the main view */
  Kplot3dView(KApplication* a=0,QWidget *parent = 0);
  /** Destructor for the main view */
  ~Kplot3dView();
  DataRanges data() { plotField->getRanges(rdata ); return rdata; }
  void setData( DataRanges data) { rdata = data; plotField->setRanges(data);}

public slots:
  void saveImg();
  void saveData();
	void slotOptions();
	void slotSetColor();
	void slotDone_perc(int perc);
	void setLine(int line) { /*funcStr->setCursorPosition(line); */};
	void slotOptionsAxes();
signals:
	void showStatus(const char* message);	
private:
  QPushButton *repaintButton;
  QComboBox  *funcStr;
	OptionsDialog *options;
	DataRanges rdata;
	
//	QColor upColor, downColor;
public:
	PlotField    *plotField;	

};

#endif // KPLOT3DVIEW_H 


























