#ifndef SetColorData_H
#define SetColorData_H

#include <qdialog.h>
#include <qslider.h>
#include <qcolor.h>
#include "ShowColor.h"

class SetColor : public QDialog
{
    Q_OBJECT
    
public:

    SetColor ( QWidget* parent , QColor& cUp, QColor& cDown,
                QColor& cLinesUp, QColor& cLinesDown, int contrast, const char* name = NULL );
    ~SetColor();
    QColor cUp();
    QColor cDown();
    QColor cLinesUp();
    QColor cLinesDown();
    int contrast();


private:
	QSlider * Slider_red, *Slider_green, *Slider_blue, *Slider_contrast;
	ShowColor * showColor;

};


#endif // SetColorData_H

