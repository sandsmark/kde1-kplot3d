/***************************************************************************
                          FILENAME  -  description                              
                             -------------------                                         
    begin                : DATE                                           
    copyright            : (C) YEAR by AUTHOR                         
    email                : EMAIL                                     
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   * 
 *                                                                         *
 ***************************************************************************/

#include "axesoptions.h"

AxesOptions::AxesOptions(QWidget *parent, const char *name) : QDialog(parent,name,true){
	initDialog();
	connect( ok_button, SIGNAL(clicked()), SLOT(accept()) );
	connect( cancel_button, SIGNAL(clicked()), SLOT(reject()) );
	connect( check_showPoints, SIGNAL(clicked()), SLOT(slotShowPoints()));
	connect( check_addAxes, SIGNAL(clicked()), SLOT(slotAddAxes()));
	connect( check_useGrid, SIGNAL(clicked()), SLOT(slotUseGrid()));
}

AxesOptions::~AxesOptions(){
}

void AxesOptions::setData( AxesData& data )
{
	QString s;
	s.setNum(data.gridStep);
	line_gridStep->setText( s.data() );
	
	s.setNum(data.pointsStep);
	line_pointsStep->setText( s.data() );
	
 	check_useGrid->setChecked( data.useGrid );
 	check_addAxes->setChecked( data.addAxes );
 	check_center ->setChecked( data.moveCenter );
  check_showBox->setChecked( data.showBox );
	check_showPoints->setChecked( data.showPoints );
	check_showNumbers->setChecked( data.showNumbers );
	
	line_pointsStep->setEnabled(check_showPoints->isChecked());
	check_showNumbers->setEnabled(check_showPoints->isChecked());
	line_gridStep->setEnabled(check_useGrid->isChecked());
	check_center->setEnabled(check_addAxes->isChecked());
	
}

void AxesOptions::getData( AxesData& data ) {
	QString s;
	
	data.useGrid = check_useGrid->isChecked();
	data.addAxes = check_addAxes->isChecked();
	data.moveCenter = check_center->isChecked();
	data.showBox = check_showBox->isChecked();
	data.showPoints = check_showPoints->isChecked();
	data.showNumbers = check_showNumbers->isChecked();
	
	s = line_gridStep->text();
	data.gridStep = s.toDouble();
	
	s = line_pointsStep->text();
	data.pointsStep = s.toDouble();
}

void AxesOptions::slotShowPoints()
{
	line_pointsStep->setEnabled(check_showPoints->isChecked());
	check_showNumbers->setEnabled(check_showPoints->isChecked());
}

void AxesOptions::slotUseGrid()
{
	line_gridStep->setEnabled(check_useGrid->isChecked());
}

void AxesOptions::slotAddAxes()
{
	check_center->setEnabled(check_addAxes->isChecked());
}