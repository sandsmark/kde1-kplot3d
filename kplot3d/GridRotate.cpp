#include "GridRotate.h"
#include "Vector.h"
#include <qpixmap.h>
#include <qcolor.h>
#include <qpainter.h>

#define PI 3.1415926

   struct Point2
   {
      int x, y;
   };


   GridRotate::GridRotate( QWidget *parent, const char *name )
   : QWidget( parent, name )
   {
      phi = psi = 0;
   }

   void GridRotate::changePhi( int newPhi )
   {
      phi = double(newPhi) *PI/180;
      repaint(); 
   }


   void GridRotate::changePsi( int newPsi )
   {
      psi = double(newPsi) *PI/180;
      repaint();
   }

   void GridRotate:: paintEvent( QPaintEvent * )
   {
      int w =   int((double)width()*0.7);
      int h =    int((double)height()*0.7);
      QPainter p, tmp;
      QPixmap *pix = new QPixmap( width() , height() );
      pix->fill( this, 1, 1 );
      tmp.begin( pix );
   
      Point2 borders[4];
      Vector bp[4];
   
      double sphi = sin(phi);
      double cphi = cos(phi);
      double spsi = sin(psi);
      double cpsi = cos(psi);
   
      Vector e1 (cphi, sphi, 0);
      Vector e2 (spsi*sphi, -spsi*cphi, cpsi);
      Vector e3 (sphi*cpsi, -cphi*cpsi, -spsi);
   
      bp[0].x = -w/2;
      bp[0].y = -h/2;
      bp[0].z = 0;
      borders[0].x = int ( bp[0] & e1 )+w/2;
      borders[0].y = int ( bp[0] & e2 )+h/2;
   
      bp[1].x = w/2;
      bp[1].y = -h/2;
      bp[1].z = 0;
      borders[1].x = int ( bp[1] & e1 )+w/2;
      borders[1].y = int ( bp[1] & e2 )+h/2;
   
      bp[2].x = w/2;
      bp[2].y = h/2;
      bp[2].z = 0;
      borders[2].x = int ( bp[2] & e1 )+w/2;
      borders[2].y = int ( bp[2] & e2 )+h/2;
   
      bp[3].x = -w/2;
      bp[3].y = h/2;
      bp[3].z = 0;
      borders[3].x = int ( bp[3] & e1 )+w/2;
      borders[3].y = int ( bp[3] & e2 )+h/2;
   
      Vector  Edge1 = bp[1]-bp[0];
      Vector  Edge2 = bp[3]-bp[0];
      Vector  n = Edge1 ^ Edge2;
      QColor  pcolor;
      if ((n & e3) >=0) {
         int color = (int)(50+100*(n&e3)/!n);
         pcolor.setRgb(color,color,color);
      }
      else	
         pcolor.setRgb(0, 0, int(20-193*(n&e3)/!n) );	
   
   tmp.setPen(pcolor);
      for (int i =0 ; i<11; i++) {
         int x1= int( (borders[0].x*(10-i) + borders[1].x*i) /10 + w/0.7*0.15 );
         int x2= int( (borders[3].x*(10-i) + borders[2].x*i) /10 + w/0.7*0.15 );
         int y1= int( (borders[0].y*(10-i) + borders[1].y*i) /10 + h/0.7*0.15 );
         int y2= int( (borders[3].y*(10-i) + borders[2].y*i) /10 + h/0.7*0.15 );
         tmp.drawLine( x1, y1, x2, y2  ); // horisontal line
      
      
      
         x1= int( (borders[0].x*(10-i) + borders[3].x*i) /10  + w/0.7*0.15 );
         x2= int( (borders[1].x*(10-i) + borders[2].x*i) /10  + w/0.7*0.15 );
         y1= int( (borders[0].y*(10-i) + borders[3].y*i) /10  + h/0.7*0.15 );
         y2= int( (borders[1].y*(10-i) + borders[2].y*i) /10  + h/0.7*0.15 );
         tmp.drawLine( x1, y1, x2, y2  ); //vertical line
      
      //        debug("x1: %i     y1:%i     x2: %i       y2:%1  ", x1, y1, x2, y2);
      }
   
      tmp.end();
   
      p.begin(this);
      p.drawPixmap( 0, 0, *pix );
      p.end();
   
      delete pix;
   
   }