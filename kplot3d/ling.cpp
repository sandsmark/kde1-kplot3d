/***************************************************************************
                          ling.cpp  -  description                              
                             -------------------                                         
    begin                : Thu Apr 1 1999                                           
    copyright            : (C) 1999 by Dmitry Poplavsky                         
    email                : pdv@hpserv.rpd.univ.kiev.ua                                     
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   * 
 *                                                                         *
 ***************************************************************************/
#include <math.h>
#include <stdlib.h>
#include <ctype.h>
#include "ling.h"
#include <qregexp.h>
#include <qstring.h>

   Ling::Ling( char *s ) {
   		setFunc(s);
   }

   void Ling::setFunc ( const char * s)
   {
   		if (!s) return;
   		QString tmp(s);
   		tmp.replace( QRegExp(" "), "" );
   		strncpy(this->s, tmp.data(), 1023);
   }

   Ling::Ling() {
      this->s[0] = 0;
   }

   Ling::~Ling() { 
   }

   double Ling::calc(double x = 0 , double y = 0)
   {
      this->x = x;
      this->y = y;
      pos = 0;
      if ( s[0] == 0 ) return 0;
      double res = expr();
      if ( pos != (int)strlen(s) ) {
	      	char mesg[128] = "Unexpected symbol : ";
    	 	char chstr[2];
      		chstr[0] = s[pos] ;
      		chstr[1] = 0;
      		strncat( mesg, chstr, 128 );
      		throw (SyntaxException( mesg , pos));
      }
      return res;
   };

   char Ling::nextchar()
   {
      char ch = ' ';
      while ( ch==' ')
      {
         if ( (int)strlen(s) <= pos) {
            pos++;
            return ' ';
         }
         ch = s[pos];
         pos++;
      };
      return ch;
   }


   double Ling::expr()
   {
      char ch;
      double res = mul();
      while (((ch = nextchar()) == '+')||(ch == '-'))
      {
         if (ch=='+')
            res+=mul();
         else
            res-=mul();
      }
      pos--;
      return res;
   }


   double Ling::mul()
   {
      char ch;
      double res = num();
      while (((ch = nextchar()) == '*')||(ch == '/')||(ch == '^'))  {
         if ( ch == '*' )
            res*=num();

         if ( ch == '/' )
         {
            double tmp = num();
            if (tmp==0) throw( RangeException("Range exception : Division by zero", pos) );
            res /= tmp;
         }

         if ( ch == '^' )
         {
//         	if ( res < 0 ) throw( RangeException("Range exception : grade from negative number ", pos) );
         	res = pow( fabs(res) , num() );
         }
      }
      pos--;
      return res;
   }


   double Ling::num() {
      double res = 0;
      double sign = 1.0;
      char tmp[128] = "";
   
      char ch = nextchar();
   
      if (ch=='-'){
         sign = -1.0;
         ch = nextchar();
      }
   
      if (ch=='+') ch = nextchar();
   
      if (ch=='(') {
         res = expr();
         if (nextchar() != ')')  throw( SyntaxException("Syntax Error : ')' needed", pos) ) ;
         return res*sign;
      }
      
      // |exp| == abs(exp)
      if (ch=='|') {
         res = expr();
         if (nextchar() != '|')  throw( SyntaxException("Syntax Error : '|' needed", pos) ) ;
         return fabs(res)*sign;
      }
      
      
      // [exp] == int(exp)
      if (ch=='[') {
         res = expr();
         if (nextchar() != ']')  throw( SyntaxException("Syntax Error : ']' needed", pos) ) ;
         return double(int(res))*sign;
      }
      
      // {exp} == exp - int(exp)
      if (ch=='{') {
         res = expr();
         if (nextchar() != '}')  throw( SyntaxException("Syntax Error : '}' needed", pos) ) ;
         return ( res - double(int(res)) )*sign;
      }
      
   
      if ( tolower(ch) == 'x' )  return x*sign;
      if ( tolower(ch) == 'y' )  return y*sign;
      if ( tolower(ch) == 'r' )  return (sqrt(x*x+y*y))*sign;
      
     // Get digit  ekvivalent
   
      if (isdigit(ch)) {
         tmp[1] = 0;
         tmp[0] = ch;
	 int i = 1;
         while ( isdigit( ch = nextchar() ) || ( ch=='.') ) {
	    tmp[i] = ch;
	    i++;
	    if ( i > 122 ) break;
	 }
	 tmp[i] = 0;
         char ch1 = nextchar();
         if  ( (tolower(ch)=='e') && ( (ch1=='+')||(ch1=='-')|| isdigit(ch1)) ) {
            tmp[i] = ch;
            tmp[i+1] = ch1;
	    i+=2;
	    tmp[i] = 0;
            while ( isdigit( ch = nextchar() ) ) {
	        tmp[i] = ch;
		i++;
		if ( i > 125 ) break;
	    }
	    tmp[i] = 0;
         } 
         else pos--;
         res = atof( tmp );
         pos--;
         return res*sign;
      };
   
      if ( isalpha(ch) ) {
        tmp[0] = tolower(ch);
        tmp[1] = 0;
	     int i = 1;
	     int funcPos = pos;  // pos of begin of function, need if function unknown
         while ( isalpha(ch = nextchar())) tmp[i++] = tolower(ch);
	     tmp[i] = 0;
	     if ( strcmp(tmp,"pi") == 0 ) return PI*sign;
	     if ( strcmp(tmp,"e") == 0 ) return exp(1)*sign;
	
         if (ch=='(') {

         	if ( strcmp(tmp,"file") == 0 ) {
         		char  fname[512];
         		int i = 0;
         		while ( (ch = nextchar()) != ')' ) fname[i++] = tolower(ch);
         		fname[i] = 0;
         		res = getData.calc(fname,x,y);
         		pos--;
         	} else {
        	  double	t_exp = expr();
        	 	if ( strcmp(tmp,"h") == 0 ){
              res = ( t_exp > 0 ) ? 1 : 0 ;
            } else
            if ( strcmp(tmp,"hsin") == 0 ){
              double ex = exp(t_exp);
              res = 0.5*(ex-1/ex);
            } else
            if ( strcmp(tmp,"hcos") == 0 ){
              double ex = exp(t_exp);
              res = 0.5*(ex+1/ex);
            } else
            if ( strcmp(tmp,"htan") == 0 ){
              double ex = exp(t_exp);
              double ex1 = 1/ex;
              res = (ex-ex1)/(ex+ex1);
            } else
            if ( strcmp(tmp,"sin") == 0 ) res = sin(t_exp); else
            if ( strcmp(tmp,"cos") == 0 ) res = cos(t_exp);  else
            if ( strcmp(tmp,"exp") == 0 ) res = exp(t_exp);  else
            if ( strcmp(tmp,"atan") == 0 ) res = atan(t_exp);  else
            if ( strcmp(tmp,"asin") == 0 ) {
            		if ( fabs( t_exp ) > 1 )
            			throw( RangeException("Range exception : asin() ", pos) );
            		res = asin(t_exp);	
            } else
            if ( strcmp(tmp,"acos") == 0 ) {
            		if ( fabs( t_exp ) > 1 )
            			throw( RangeException("Range exception : asin() ", pos) );
            		res = acos(t_exp);	
            } else


	    if ( strcmp(tmp,"sqr") == 0 ) res = t_exp * t_exp;  else
            if ( strcmp(tmp,"sqrt")== 0 ) {
            if ( t_exp >=0 )
	       res = sqrt( t_exp );
	    else
               throw( RangeException("Range exception : sqrt( ...<0 )", pos) );
            } else
            if ( strcmp(tmp,"ln") == 0 ) {
                if ( t_exp > 0 )
                  res = log( t_exp );
                else
                  throw( RangeException("Range exception : ln( ...<=0 )", pos) );
            } else
            if ( strcmp(tmp,"lg") == 0 ) {
        	if ( t_exp > 0 )
                     res = log( t_exp )/log( 10 );
                else
                     throw( RangeException("Range exception : lg( ...<=0 )", pos) );
            } else
            if ( strcmp(tmp,"tan") == 0 ) {
		double cexp = cos(t_exp);
		if ( cexp == 0 ) 
		    throw( RangeException("Range exception : tan() = oo", pos) );
		else  res = sin(t_exp)/cexp;
            } else
            if ( strcmp(tmp,"min") == 0 ) {
                res = t_exp;
        	while( nextchar()==',' ) {
            	    t_exp = expr();
            	    if ( res > t_exp ) res = t_exp;
                }     
                pos--;
            } else
            if ( strcmp(tmp,"max") == 0 ) {
               res = t_exp;
               while( nextchar()==',' ) {
                  t_exp = expr();
                  if ( res < t_exp ) res = t_exp;
               }     
               pos--;
            }
            else {
		char mesg[150] = "Syntax Error :  unknown function : ";
		strncat(mesg,tmp,149);
		throw (SyntaxException( mesg , funcPos));
	    } }
            if (nextchar()!=')') throw( SyntaxException("Syntax Error : ')' needed", pos) ) ;
         } else throw( SyntaxException("Syntax Error : '(' needed", pos-1));
         return res*sign;
      }
      char mesg[128] = "Unexpected symbol : ";
      char chstr[2];
      chstr[0] = ch;
      chstr[1] = 0;
      strncat( mesg, chstr, 128 );
      throw (SyntaxException( mesg , pos));
   }



