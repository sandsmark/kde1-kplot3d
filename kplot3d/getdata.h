/***************************************************************************
                          FILENAME  -  description                              
                             -------------------                                         
    begin                : DATE                                           
    copyright            : (C) YEAR by AUTHOR                         
    email                : EMAIL                                     
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   * 
 *                                                                         *
 ***************************************************************************/


#ifndef GETDATA_H
#define GETDATA_H

#include <qdict.h>
#include <qarray.h>

class Exception;
class SyntaxException;
class RangeException;

struct Point3
{
   float x, y, z;
};



class GetDataFile {
public:
	GetDataFile( char *fname );
	~GetDataFile();
	float calc( float x, float y );
private:
	Point3 * dtable; // points , read from file
//  int * ttable;  // array of triangles, indexes of dtable
  int lastTriangle;
  bool inTriagle ( float x, float y, int tr );
  float approx ( float x, float y, int tr );
  float res;
  int tableSize, trSize;
  void readFile(char * fname);

};


/**Need for Ling class
Read data from file in xyz format
and approx. data between points
  *@author Dmitry Poplavsky
  */

class GetData {
public: 
	GetData();
	~GetData();
	double calc ( char *fname, double x, double y);
private:
	QDict<GetDataFile> dict;
};

#endif
