#ifndef OPTIONS_H
#define OPTIONS_H


#include <qdialog.h>
#include <qlabel.h>
#include <qpushbutton.h>
#include <qlcdnumber.h>
#include <qscrollbar.h>
#include <qlineedit.h>
#include <qframe.h>
#include "DataRanges.h"




class OptionsDialog : public QDialog {
Q_OBJECT
public:


   OptionsDialog( QWidget *parent );
   ~OptionsDialog () {};
   void setData(DataRanges data);
   void getData(DataRanges &data);
   QLineEdit *minX, *maxX, *minY, *maxY;
   QLineEdit *surf_nx, *surf_ny, *rot_nx, *rot_ny, *line_nx, *line_ny;
   QScrollBar* ScrollBar_phi;
   QScrollBar* ScrollBar_psi;
};

#endif