#ifndef SHOWCOLOR_H
#define SHOWCOLOR_H

#include <qwidget.h>
#include <qcolor.h>

class ShowColor : public QWidget
{
   Q_OBJECT
public:
	ShowColor ( QWidget* parent , QColor&  cUp , QColor&  cDown,
	           QColor&  cLinesUp , QColor&  cLinesDown  );
	void setRgb (int R, int G, int B);
	QColor cUp() { return colors[0]; }
	QColor cDown() { return colors[1]; }
	QColor cLinesUp() { return colors[2]; }
	QColor cLinesDown() { return colors[3]; }

public slots:
	void setR ( int R ) ;
	void setG ( int G );
	void setB  ( int B );
	void setDefault();

	void swichColor( int c ) {
		cflag = c;
		r = colors[cflag+tflag].red();
		g = colors[cflag+tflag].green();
		b = colors[cflag+tflag].blue();
		emit changedR(r);
		emit changedG(g);
		emit changedB(b);
	};	
	
	void swichType( int c ) {
		tflag = c*2;
		r = colors[cflag+tflag].red();
		g = colors[cflag+tflag].green();
		b = colors[cflag+tflag].blue();
		emit changedR(r);
		emit changedG(g);
		emit changedB(b);
	};
	
	void toGray() {  setRgb(128,128,128); }
	void toGreen() {  setRgb(0,128,0); }
	void toBlue() {  setRgb(0,0,128); }
	void toMag() {  setRgb(128,0,128); }
	
signals:
	void changedR(int);
	void changedG(int);
	void changedB(int);	
	
protected:
        void paintEvent( QPaintEvent * );	

private:
	QColor colors[4];
	int r,g,b;
	int cflag,tflag;  // upper or lower color choosed
	
};
#endif


