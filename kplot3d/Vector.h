// Vector.h
#include <math.h>

class Vector
{
  public:
  double x, y, z;
  Vector () {};
  Vector (double f) {x = y = z = f; };
  Vector (const Vector& v ) { x=v.x; y=v.y; z=v.z; };
  Vector (double vx, double vy, double vz ) { x=vx; y=vy; z=vz; }
  Vector& operator = (const Vector& v) { x=v.x; y=v.y; z=v.z; return *this; };
  Vector& operator = (double f) {x = y = z = f; return *this; }; 
  Vector operator - () const {return Vector (-x,-y,-z);};
//  Vector& operator += (const Vector& v); { x+=v.x; y+=v.y; z+=v.z; return this; };
//  Vector& operator -= (const Vector& v); { x-=v.x; y-=v.y; z-=v.z; return this; };
//  Vector& operator *= (const Vector& v); { x*=v.x; y*=v.y; z*=v.z; return this; };
//  Vector& operator *= ( double d); { x*=d; y*=d; z*=d; return this; };
//  Vector& operator /= ( double d); { x*=d; y*=d; z*=d; return this; };
  friend Vector operator + ( const Vector&, const Vector&);
  friend Vector operator - ( const Vector&, const Vector&);
  friend Vector operator * ( const Vector&, const Vector&);
  friend Vector operator * ( double, const Vector&);
  friend Vector operator * ( const Vector&, double);
  friend Vector operator / ( const Vector&, double);
  friend Vector operator / ( const Vector&, const Vector&);
  friend double operator & ( const Vector& u, const Vector& v)
	        { return u.x*v.x + u.y*v.y + u.z*v.z; };
  friend Vector operator ^ ( const Vector&, const Vector&);			
  double operator ! () { return (double) sqrt (x*x+y*y+z*z); };
  double operator [] (int n) { return (n==1?x:(n==2?y:z));  };
};

inline Vector operator + (const Vector& u, const Vector& v) {
 return Vector(u.x+v.x, u.y+v.y, u.z+v.z);
};

inline Vector operator - (const Vector& u, const Vector& v) {
 return Vector(u.x-v.x, u.y-v.y, u.z-v.z);
};
  
inline Vector operator * (const Vector& u, const Vector& v) {
 return Vector(u.x*v.x, u.y*v.y, u.z*v.z);
};

inline Vector operator * (const Vector& u, double f) {
 return Vector(u.x*f, u.y*f, u.z*f);
};

inline Vector operator * (double f, const Vector& u) {
 return Vector(u.x*f, u.y*f, u.z*f);
};

inline Vector operator ^ (const Vector& u, const Vector& v) {
  return Vector ( u.y*v.z-u.z*v.y, u.z*v.x-u.x*v.z, u.x*v.y-v.x*u.y);
};
