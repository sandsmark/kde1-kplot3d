#include <qpixmap.h>
#include <qlayout.h>
#include <qlabel.h>
#include <qradiobutton.h>
#include <qpushbutton.h>
#include <qslider.h>
#include <qlcdnumber.h>
#include <qbuttongroup.h>
#include <qwidget.h>
#include <qbrush.h>
#include <qpainter.h>

#include <kapp.h>

#include "SetColor.h"
#include "ShowColor.h"

SetColor::SetColor ( QWidget* parent, QColor& cUp, QColor& cDown,
     QColor& cLinesUp, QColor& cLinesDown,int contrast, const char* name )
	: QDialog( parent, name, TRUE, 20480 )
{
    resize( 380,370 );
    setCaption(i18n("plot3d : choose colors"));
	
	QButtonGroup* bg, *bg2;
	bg = new QButtonGroup( this, "bg" );
	bg->setGeometry( 200, 140, 150, 80 );
	bg->setFocusPolicy( QWidget::NoFocus );
	bg->setFrameStyle( 49 );
	bg->setLineWidth( 1 );
	bg->setMidLineWidth( 0 );
	bg->QFrame::setMargin( 0 );
	bg->setTitle( "" );
	bg->setAlignment( 1 );
	
	bg2 = new QButtonGroup( this, "bg2" );
	bg2->setGeometry( 200, 230, 150, 80 );
	bg2->setFocusPolicy( QWidget::NoFocus );
	bg2->setFrameStyle( 49 );
	bg2->setLineWidth( 1 );
	bg2->setMidLineWidth( 0 );
	bg2->QFrame::setMargin( 0 );
	bg2->setTitle( "" );
	bg2->setAlignment( 1 );

	QFrame* Frame_1;
	Frame_1 = new QFrame( this, "Frame_1" );
	Frame_1->setGeometry( 10, 10, 170, 300 );
	Frame_1->setMinimumSize( 0, 0 );
	Frame_1->setMaximumSize( 32767, 32767 );
	Frame_1->setFocusPolicy( QWidget::NoFocus );
	Frame_1->setBackgroundMode( QWidget::PaletteBackground );
	Frame_1->setFontPropagation( QWidget::NoChildren );
	Frame_1->setPalettePropagation( QWidget::NoChildren );
	Frame_1->setFrameStyle( 33 );
	Frame_1->setLineWidth( 1 );
	Frame_1->setMidLineWidth( 0 );
	Frame_1->QFrame::setMargin( 0 );


	QLCDNumber* LCD_red;
	LCD_red = new QLCDNumber( this, "LCD_red" );
	LCD_red->setGeometry( 90, 20, 80, 30 );
	LCD_red->setNumDigits( 3 );

	QLCDNumber* LCD_blue;
	LCD_blue = new QLCDNumber( this, "LCD_blue" );
	LCD_blue->setGeometry( 90, 180, 80, 30 );
	LCD_blue->setNumDigits( 3 );

	QLCDNumber* LCD_green;
	LCD_green = new QLCDNumber( this, "LCD_green" );
	LCD_green->setGeometry( 90, 100, 80, 30 );
	LCD_green->setNumDigits( 3 );


	QLabel* Label_red;
	Label_red = new QLabel( this, "Label_red" );
	Label_red->setGeometry( 20, 20, 60, 30 );
	Label_red->setText( i18n("Red :") );

	QLabel* Label_green;
	Label_green = new QLabel( this, "Label_green" );
	Label_green->setGeometry( 20, 100, 60, 30 );
	Label_green->setText( i18n("Green :") );

	QLabel* Label_blue;
	Label_blue = new QLabel( this, "Label_blue" );
	Label_blue->setGeometry( 20, 180, 60, 30 );
	Label_blue->setText( i18n("Blue :") );

	
	Slider_red = new QSlider( this, "Slider_red" );
	Slider_red->setGeometry( 20, 60, 150, 20 );
	Slider_red->setOrientation( QSlider::Horizontal );
	Slider_red->setRange( 0, 255 );
	Slider_red->setSteps( 1, 10 );
	Slider_red->setValue( 0 );
	Slider_red->setTracking( TRUE );
	
	Slider_green = new QSlider( this, "Slider_green" );
	Slider_green->setGeometry( 20, 140, 150, 20 );
	Slider_green->setOrientation( QSlider::Horizontal );
	Slider_green->setRange( 0, 255 );
	Slider_green->setSteps( 1, 10 );
	Slider_green->setValue( 0 );
	Slider_green->setTracking( TRUE );

	Slider_blue = new QSlider( this, "Slider_blue" );
	Slider_blue->setGeometry( 20, 220, 150, 20 );
	Slider_blue->setOrientation( QSlider::Horizontal );
	Slider_blue->setRange( 0, 255 );
	Slider_blue->setSteps( 1, 10 );
	Slider_blue->setValue( 0 );
	Slider_blue->setTracking( TRUE );

	QRadioButton* RadioButton_up;
	RadioButton_up = new QRadioButton( this );
	RadioButton_up->setGeometry( 220, 150, 100, 20 );
	RadioButton_up->setText( i18n("Up color") );
	RadioButton_up->setChecked( TRUE );

	QRadioButton* RadioButton_down;
	RadioButton_down = new QRadioButton( this );
	RadioButton_down->setGeometry( 220, 190, 100, 20 );
	RadioButton_down->setText( i18n("Down color") );
	RadioButton_down->setChecked( FALSE );
	
	QRadioButton* RadioButton_Shades;
	RadioButton_Shades = new QRadioButton( this );
	RadioButton_Shades->setGeometry( 220, 240, 100, 20 );
	RadioButton_Shades->setText( i18n("Shades") );
	RadioButton_Shades->setChecked( TRUE );

	QRadioButton* RadioButton_Lines;
	RadioButton_Lines = new QRadioButton( this );
	RadioButton_Lines->setGeometry( 220, 280, 100, 20 );
	RadioButton_Lines->setText( i18n("Lines") );
	RadioButton_Lines->setChecked( FALSE );


	showColor = new ShowColor( this, cUp, cDown,cLinesUp, cLinesDown );
	showColor->setGeometry( 200, 20, 60, 50 );
	showColor->setFocusPolicy( QWidget::NoFocus );

	QPushButton* Button_gray;
	Button_gray = new QPushButton( this, "Button_gray" );
	Button_gray->setGeometry( 240, 90, 30, 30 );
	Button_gray->setText( "" );
	{
		QColorGroup normal( QColor( QRgb(0) ), QColor( QRgb(10789024) ), QColor( QRgb(16777215) ), QColor( QRgb(8421504) ), QColor( QRgb(10789024) ), QColor( QRgb(0) ), QColor( QRgb(16777215) ) );
		QColorGroup disabled( QColor( QRgb(8421504) ), QColor( QRgb(12632256) ), QColor( QRgb(16777215) ), QColor( QRgb(8421504) ), QColor( QRgb(10789024) ), QColor( QRgb(8421504) ), QColor( QRgb(12632256) ) );
		QColorGroup active( QColor( QRgb(0) ), QColor( QRgb(12632256) ), QColor( QRgb(16777215) ), QColor( QRgb(8421504) ), QColor( QRgb(10789024) ), QColor( QRgb(0) ), QColor( QRgb(16777215) ) );
		QPalette palette( normal, disabled, active );
		Button_gray->setPalette( palette );
	}
	

	QPushButton* Button_blue;
	Button_blue = new QPushButton( this, "Button_blue" );
	Button_blue->setGeometry( 200, 90, 30, 30 );
	Button_blue->setText( "" );
	{
		QColorGroup normal( QColor( QRgb(0) ), QColor( QRgb(8388608) ), QColor( QRgb(16777215) ), QColor( QRgb(8421504) ), QColor( QRgb(10789024) ), QColor( QRgb(0) ), QColor( QRgb(16777215) ) );
		QColorGroup disabled( QColor( QRgb(8421504) ), QColor( QRgb(12632256) ), QColor( QRgb(16777215) ), QColor( QRgb(8421504) ), QColor( QRgb(10789024) ), QColor( QRgb(8421504) ), QColor( QRgb(12632256) ) );
		QColorGroup active( QColor( QRgb(0) ), QColor( QRgb(12632256) ), QColor( QRgb(16777215) ), QColor( QRgb(8421504) ), QColor( QRgb(10789024) ), QColor( QRgb(0) ), QColor( QRgb(16777215) ) );
		QPalette palette( normal, disabled, active );
		Button_blue->setPalette( palette );
	}
	
	
	QPushButton* Button_mag;
	Button_mag = new QPushButton( this, "Button_mag" );
	Button_mag->setGeometry( 280, 90, 30, 30 );
	Button_mag->setText( "" );
	{
		QColorGroup normal( QColor( QRgb(0) ), QColor( QRgb(8388736) ), QColor( QRgb(16777215) ), QColor( QRgb(8421504) ), QColor( QRgb(10789024) ), QColor( QRgb(0) ), QColor( QRgb(16777215) ) );
		QColorGroup disabled( QColor( QRgb(8421504) ), QColor( QRgb(12632256) ), QColor( QRgb(16777215) ), QColor( QRgb(8421504) ), QColor( QRgb(10789024) ), QColor( QRgb(8421504) ), QColor( QRgb(12632256) ) );
		QColorGroup active( QColor( QRgb(0) ), QColor( QRgb(12632256) ), QColor( QRgb(16777215) ), QColor( QRgb(8421504) ), QColor( QRgb(10789024) ), QColor( QRgb(0) ), QColor( QRgb(16777215) ) );
		QPalette palette( normal, disabled, active );
		Button_mag->setPalette( palette );
	}

	QPushButton* Button_green;
	Button_green = new QPushButton( this, "Button_green" );
	Button_green->setGeometry( 320, 90, 30, 30 );
	Button_green->setText( "" );
	{
		QColorGroup normal( QColor( QRgb(0) ), QColor( QRgb(32768) ), QColor( QRgb(16777215) ), QColor( QRgb(8421504) ), QColor( QRgb(10789024) ), QColor( QRgb(0) ), QColor( QRgb(16777215) ) );
		QColorGroup disabled( QColor( QRgb(8421504) ), QColor( QRgb(12632256) ), QColor( QRgb(16777215) ), QColor( QRgb(8421504) ), QColor( QRgb(10789024) ), QColor( QRgb(8421504) ), QColor( QRgb(12632256) ) );
		QColorGroup active( QColor( QRgb(0) ), QColor( QRgb(12632256) ), QColor( QRgb(16777215) ), QColor( QRgb(8421504) ), QColor( QRgb(10789024) ), QColor( QRgb(0) ), QColor( QRgb(16777215) ) );
		QPalette palette( normal, disabled, active );
		Button_green->setPalette( palette );
	}
	
	QPushButton* Button_default;
	Button_default = new QPushButton( this, "Button_default" );
	Button_default->setGeometry( 276, 30, 77, 30 );
	Button_default->setText( i18n("default") );


	QPushButton* Button_OK;
	Button_OK = new QPushButton( this, "Button_OK" );
	Button_OK->setGeometry( 140, 320, 100, 30 );
	Button_OK->setText( i18n("OK") );
	Button_OK->setDefault( TRUE );

	QPushButton* Button_cancel;
	Button_cancel = new QPushButton( this, "Button_cancel" );
	Button_cancel->setGeometry( 260, 320, 100, 30 );
	Button_cancel->setText( i18n("Cancel") );

	bg->insert( RadioButton_up );
	bg->insert( RadioButton_down );
	
	bg2->insert( RadioButton_Shades );
	bg2->insert( RadioButton_Lines );
	
	
	
	Slider_contrast = new QSlider( this, "Slider_contrast" );
	Slider_contrast->setGeometry( 20, 280, 151, 20 );
	Slider_contrast->setRange( 0, 99 );
	Slider_contrast->setSteps( 1, 10 );
	Slider_contrast->setOrientation( QSlider::Horizontal );
	Slider_contrast->setValue( contrast );
	
	QLabel* Label_15;
	Label_15 = new QLabel( this, "Label_15" );
	Label_15->setGeometry( 30, 250, 131, 20 );
	Label_15->setText( "Contrast:" );
	Label_15->setAlignment( 292 );
	

	connect ( bg, SIGNAL(clicked(int)), showColor, SLOT(swichColor(int)));
	connect ( bg2, SIGNAL(clicked(int)), showColor, SLOT(swichType(int)));
	
	connect ( Slider_red, SIGNAL(valueChanged(int)), showColor, SLOT(setR(int)) );
	connect ( Slider_green, SIGNAL(valueChanged(int)), showColor, SLOT(setG(int)) );
	connect ( Slider_blue, SIGNAL(valueChanged(int)), showColor, SLOT(setB(int)) );
	
	connect (  showColor, SIGNAL(changedR(int)), Slider_red, SLOT(setValue(int)) );
	connect (  showColor, SIGNAL(changedG(int)), Slider_green, SLOT(setValue(int)) );
	connect (  showColor, SIGNAL(changedB(int)), Slider_blue, SLOT(setValue(int)) );
	
	
	connect ( Slider_red, SIGNAL(valueChanged(int)), LCD_red, SLOT(display(int)));
	connect ( Slider_green, SIGNAL(valueChanged(int)), LCD_green, SLOT(display(int)));
	connect ( Slider_blue, SIGNAL(valueChanged(int)), LCD_blue, SLOT(display(int)));
	
	connect ( Button_gray, SIGNAL(clicked()), showColor, SLOT(toGray()));
	connect ( Button_green, SIGNAL(clicked()), showColor, SLOT(toGreen()));
	connect ( Button_blue, SIGNAL(clicked()), showColor, SLOT(toBlue()));
	connect ( Button_mag, SIGNAL(clicked()), showColor, SLOT(toMag()));
	connect ( Button_default, SIGNAL(clicked()), showColor, SLOT(setDefault()));
	
	connect ( Button_OK, SIGNAL(clicked()), SLOT(accept()) );
	connect( Button_cancel, SIGNAL(clicked()), SLOT(reject()) );

	Slider_red->setValue( cUp.red() );	
	Slider_green->setValue( cUp.green() );	
	Slider_blue->setValue( cUp.blue() );	
		
	
}

QColor SetColor::cUp()
{
	return showColor->cUp();
}

QColor SetColor::cDown()
{
	return showColor->cDown();
}
QColor SetColor::cLinesUp()
{
	return showColor->cLinesUp();
}

QColor SetColor::cLinesDown()
{
	return showColor->cLinesDown();
}

int SetColor::contrast()
{
	return Slider_contrast->value();
}


SetColor::~SetColor()
{
}

//            ------------           ShowColor         --------------










