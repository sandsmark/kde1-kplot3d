/***************************************************************************
                          main.cpp  -  description                              
                             -------------------                                         
    begin                : ��� ��� 11 15:22:15 EEST 1999
                                           
    copyright            : (C) 1999 by Dmitry Poplavsky                         
    email                : dima@linuxfan.com                                     
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   * 
 *                                                                         *
 ***************************************************************************/


#include "kplot3d.h" 
 
int main(int argc, char* argv[]) { 
  KApplication app(argc,argv,"kplot3d");  
 
  if (app.isRestored())
    { 
      RESTORE(Kplot3dApp);
    }
  else 
    {
      Kplot3dApp* kplot3d = new Kplot3dApp;
      kplot3d->show();
    }  
  return app.exec();
}  
 


