#include "OptionsDialog.h"
#include <qwidget.h>
#include <qdialog.h>
#include <qlabel.h>
#include <qpushbutton.h>
#include <qlcdnumber.h>
#include <qscrollbar.h>
#include <qlineedit.h>
#include <qframe.h>
#include <qvalidator.h>
#include "DataRanges.h"
#include "GridRotate.h"

#include <kapp.h>



OptionsDialog::OptionsDialog(QWidget *parent)
      :QDialog(parent,"Options",TRUE)
{

	QPushButton *ok ,*cancel;

	resize(490,330);
	setCaption("plot3d : ranges  ");
	ok = new QPushButton( "Ok", this );
	ok->setGeometry( 250, 280, 100, 30 );
	ok->setDefault(true);
	connect( ok, SIGNAL(clicked()), SLOT(accept()) );
	cancel = new QPushButton( i18n("Cancel") ,this );
	cancel->setGeometry( 370, 280, 100, 30 );
	connect( cancel, SIGNAL(clicked()), SLOT(reject()) );
	
	
	 QFrame*  Frame_1;
	 Frame_1 = new QFrame( this, "Frame_1" );
	 Frame_1->setGeometry( 20, 20, 210, 290 );
	 Frame_1->setFrameStyle( 49 );
	 Frame_1->setLineWidth( 1 );
	 Frame_1->setMidLineWidth( 0 );
	 Frame_1->QFrame::setMargin( 0 );

	QLabel* lminX;
	lminX = new QLabel( this, "lminX" );
	lminX->setGeometry( 40, 40, 60, 20 );
	lminX->setText( "Min X:" );
	lminX->setAlignment( 276 );
	

	minX = new QLineEdit( this, "minX" );
	minX->setGeometry( 110, 40, 70, 20 );
	minX->setValidator( new QDoubleValidator(-1e12, 1e12, 10,minX) );
	minX->setText( "" );
	
	QLabel* lmaxX;
	lmaxX = new QLabel( this, "lmaxX" );
	lmaxX->setGeometry( 40, 70, 60, 20 );
	lmaxX->setText( "Max X:" );
	lmaxX->setAlignment( 292 );

	maxX = new QLineEdit( this, "maxX" );
	maxX->setGeometry( 110, 70, 70, 20 );
	maxX->setValidator( new QDoubleValidator(-1e12, 1e12, 10,maxX) );
	maxX->setText( "" );
	
	QLabel* lminY;
	lminY = new QLabel( this, "lminY" );
	lminY->setGeometry( 40, 110, 60, 20 );
	lminY->setText( "Min Y:" );
	lminY->setAlignment( 292 );


	minY = new QLineEdit( this, "minY" );
	minY->setGeometry( 110, 110, 70, 20 );
	minY->setValidator( new QDoubleValidator(-1e12, 1e12, 10,minY) );
	minY->setText( "" );

	QLabel* lmaxY;
	lmaxY = new QLabel( this, "lmaxY" );
	lmaxY->setGeometry( 40, 140, 60, 20 );
	lmaxY->setText( "Max Y:" );
	lmaxY->setAlignment( 292 );


	maxY = new QLineEdit( this, "maxY" );
	maxY->setGeometry( 110, 140, 70, 20 );
	maxY->setValidator( new QDoubleValidator(-1e12, 1e12, 10,maxY) );
	maxY->setText( "" );

	QFrame* Frame_2;
	Frame_2 = new QFrame( this, "Frame_2" );
	Frame_2->setGeometry( 250, 20, 210, 240 );
	Frame_2->setFrameStyle( 49 );
	Frame_2->setLineWidth( 1 );
	Frame_2->setMidLineWidth( 0 );
	Frame_2->QFrame::setMargin( 0 );


	GridRotate* grid = new  GridRotate( this );
	grid->setGeometry( 270, 30, 150, 130 );
	

	// change phi
	ScrollBar_phi = new QScrollBar( this, "ScrollBar_phi" );
	ScrollBar_phi->setGeometry( 270, 170, 150, 20 );
	ScrollBar_phi->setOrientation( QScrollBar::Horizontal );
	ScrollBar_phi->setRange( 0, 360 );
	ScrollBar_phi->setSteps( 1, 10 );
	ScrollBar_phi->setValue( 0 );
	ScrollBar_phi->setTracking( TRUE );

    // change psi
	ScrollBar_psi = new QScrollBar( this, "ScrollBar_psi" );
	ScrollBar_psi->setGeometry( 430, 30, 20, 130 );
	ScrollBar_psi->setOrientation( QScrollBar::Vertical );
	ScrollBar_psi->setRange( -90 , 90 );
	ScrollBar_psi->setSteps( 1, 10 );
	ScrollBar_psi->setValue( 0 );
	ScrollBar_psi->setTracking( TRUE );

	QLabel* lPhi;
	lPhi = new QLabel( this, "lPhi" );
	lPhi->setGeometry( 270, 210, 30, 30 );
	lPhi->setText( "Phi:" );
	lPhi->setAlignment( 292 );

	QLCDNumber* LCDNumber_phi;
	LCDNumber_phi = new QLCDNumber( this, "LCDNumber_phi" );
	LCDNumber_phi->setGeometry( 310, 210, 40, 30 );
	LCDNumber_phi->setMinimumSize( 0, 0 );
	LCDNumber_phi->setMaximumSize( 32767, 32767 );
	LCDNumber_phi->setFocusPolicy( QWidget::NoFocus );
	LCDNumber_phi->setFrameStyle( 33 );
	LCDNumber_phi->setLineWidth( 1 );
	LCDNumber_phi->setMidLineWidth( 0 );
	LCDNumber_phi->QFrame::setMargin( 0 );
	LCDNumber_phi->setSmallDecimalPoint( FALSE );
	LCDNumber_phi->setNumDigits( 4 );
	LCDNumber_phi->setMode( QLCDNumber::DEC );
	LCDNumber_phi->setSegmentStyle( QLCDNumber::Outline );

	QLabel* lPsi;
	lPsi = new QLabel( this, "lPsi" );
	lPsi->setGeometry( 360, 210, 31, 30 );
	lPsi->setText( "Psi:" );
	lPsi->setAlignment( 292 );
	
	QLCDNumber* LCDNumber_psi;
	LCDNumber_psi = new QLCDNumber( this, "LCDNumber_psi" );
	LCDNumber_psi->setGeometry( 400, 210, 40, 30 );
	LCDNumber_psi->setMinimumSize( 0, 0 );
	LCDNumber_psi->setMaximumSize( 32767, 32767 );
	LCDNumber_psi->setFocusPolicy( QWidget::NoFocus );
	LCDNumber_psi->setFrameStyle( 33 );
	LCDNumber_psi->setLineWidth( 1 );
	LCDNumber_psi->setMidLineWidth( 0 );
	LCDNumber_psi->QFrame::setMargin( 0 );
	LCDNumber_psi->setSmallDecimalPoint( FALSE );
	LCDNumber_psi->setNumDigits( 4 );
	LCDNumber_psi->setMode( QLCDNumber::DEC );
	LCDNumber_psi->setSegmentStyle( QLCDNumber::Outline );

	QLabel* lnx;
	lnx = new QLabel( this, "lnx" );
	lnx->setGeometry( 100, 180, 40, 20 );
	lnx->setText( "N X:" );
	lnx->setAlignment( 292 );
	
	surf_nx = new QLineEdit( this, "nx" );
	surf_nx->setGeometry( 100, 210, 50, 20 );
	surf_nx->setValidator( new QIntValidator(20,400,surf_nx) );
	surf_nx->setText( "" );
	
	

	QLabel* lny;
	lny = new QLabel( this, "lny" );
	lny->setGeometry( 160, 180, 40, 20 );
	lny->setText( "N Y:" );
	lny->setAlignment( 292 );

	
	surf_ny = new QLineEdit( this, "ny" );
  surf_ny->setGeometry( 160, 210, 51, 20 );
	surf_ny->setValidator( new QIntValidator(20,400,surf_ny) );
	surf_ny->setText( "" );
	
	
	rot_nx = new QLineEdit( this, "rot_nx" );
	rot_nx->setGeometry( 100, 240, 50, 20 );
	
	rot_ny = new QLineEdit( this, "rot_ny" );
	rot_ny->setGeometry( 160, 240, 50, 20 );
	
	line_nx = new QLineEdit( this, "line_nx" );
	line_nx->setGeometry( 100, 270, 50, 20 );
	
	line_ny = new QLineEdit( this, "line_ny" );
	line_ny->setGeometry( 160, 270, 50, 20 );
	
	QLabel* label_shaded;
	label_shaded = new QLabel( this, "label_shaded" );
	label_shaded->setGeometry( 30, 210, 60, 20 );
	label_shaded->setText( "Shaded :" );
	label_shaded->setAlignment( 289 );

	QLabel* label_rotate;
	label_rotate = new QLabel( this, "label_rotate" );
	label_rotate->setGeometry( 30, 240, 61, 20 );
	label_rotate->setMinimumSize( 0, 0 );
	label_rotate->setText( "Rotate :" );
	label_rotate->setAlignment( 289 );

	QLabel* label_line;
	label_line = new QLabel( this, "label_line" );
	label_line->setGeometry( 30, 270, 60, 20 );
	label_line->setText( "Lines  :" );
	label_line->setAlignment( 289 );

	
	
	
	connect( ScrollBar_phi, SIGNAL(valueChanged(int)), LCDNumber_phi, SLOT( display(int)));
	connect( ScrollBar_psi, SIGNAL(valueChanged(int)), LCDNumber_psi, SLOT( display(int)));
	connect( ScrollBar_phi, SIGNAL(valueChanged(int)), grid , SLOT( changePhi(int)));
	connect( ScrollBar_psi, SIGNAL(valueChanged(int)), grid, SLOT( changePsi(int)));
	
	
}

void OptionsDialog::setData(DataRanges data)
{
   QString s;

   s.sprintf( "%g" , data.x1 );
   minX->setText( s );

   s.sprintf( "%g" , data.x2 );
   maxX->setText( s );

   s.sprintf( "%g" , data.y1 );
   minY->setText( s );

   s.sprintf( "%g" , data.y2 );
   maxY->setText( s );

   s.sprintf( "%i" , data.surf_nx );
   surf_nx->setText( s );

   s.sprintf( "%i" , data.surf_ny );
   surf_ny->setText( s );

   s.sprintf( "%i" , data.rot_nx );
   rot_nx->setText( s );

   s.sprintf( "%i" , data.rot_ny );
   rot_ny->setText( s );

   s.sprintf( "%i" , data.line_nx );
   line_nx->setText( s );

   s.sprintf( "%i" , data.line_ny );
   line_ny->setText( s );

   ScrollBar_phi->setValue( (int)data.phi );
   ScrollBar_psi->setValue( (int)data.psi );

}

void OptionsDialog::getData(DataRanges &data)
{
   QString tmp;

   tmp = minX->text();
   data.x1 = tmp.toDouble();

   tmp = maxX->text();
   data.x2 = tmp.toDouble();

   tmp = minY->text();
   data.y1 = tmp.toDouble();

   tmp = maxY->text();
   data.y2 = tmp.toDouble();

   tmp = surf_nx->text();
   data.surf_nx = tmp.toInt();

   tmp = surf_ny->text();
   data.surf_ny = tmp.toInt();

   tmp = rot_nx->text();
   data.rot_nx = tmp.toInt();

   tmp = rot_ny->text();
   data.rot_ny = tmp.toInt();

   tmp = line_nx->text();
   data.line_nx = tmp.toInt();

   tmp = line_ny->text();
   data.line_ny = tmp.toInt();


   data.psi = ScrollBar_psi->value();
   data.phi = ScrollBar_phi->value();

}



